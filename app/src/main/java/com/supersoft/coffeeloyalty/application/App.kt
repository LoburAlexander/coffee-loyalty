package com.supersoft.coffeeloyalty.application

import android.app.Application
import com.supersoft.coffeeloyalty.common.logger.AppLogger
import com.supersoft.coffeeloyalty.common.logger.logger
import com.supersoft.coffeeloyalty.di.Injector
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

/**
 * Created by alex.lobur on 07.09.2019.
 * Email: lobur.a.y@gmail.com
 */
class App : Application(), HasAndroidInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>
    @Inject
    lateinit var appLogger: AppLogger

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector


    override fun onCreate() {
        super.onCreate()

        initDependencyInjection()
        initLogging()
    }


    private fun initDependencyInjection() {
        Injector.init(this)
        Injector.getApplicationComponent().inject(this)
    }

    private fun initLogging() {
        logger = appLogger
    }
}