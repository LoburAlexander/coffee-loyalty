package com.supersoft.coffeeloyalty.di

import com.supersoft.coffeeloyalty.application.App
import com.supersoft.coffeeloyalty.di.component.ApplicationComponent
import com.supersoft.coffeeloyalty.di.component.DaggerApplicationComponent

/**
 * Created by alex.lobur on 07.09.2019.
 * Email: lobur.a.y@gmail.com
 */
class Injector {
    companion object {
        private lateinit var sApplicationComponent: ApplicationComponent

        fun init(app: App) {
            sApplicationComponent = DaggerApplicationComponent.builder()
                .application(app)
                .build()
        }

        fun getApplicationComponent(): ApplicationComponent {
            return sApplicationComponent
        }
    }
}