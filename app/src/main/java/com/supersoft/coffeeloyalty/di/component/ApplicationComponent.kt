package com.supersoft.coffeeloyalty.di.component

import android.app.Application
import com.supersoft.coffeeloyalty.application.App
import com.supersoft.coffeeloyalty.di.module.app.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by alex.lobur on 07.09.2019.
 * Email: lobur.a.y@gmail.com
 */
@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,

    FacadeModule::class,

    AndroidModule::class,
    LoggerModule::class,
    NavigationModule::class,
    NfcModule::class,

    ActivitiesModule::class,
    FragmentsModule::class,
    ReceiversModule::class
])
interface ApplicationComponent {
    fun inject(app: App)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }
}