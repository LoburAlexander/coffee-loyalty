package com.supersoft.coffeeloyalty.di.module.activity

import androidx.fragment.app.FragmentActivity
import com.supersoft.coffeeloyalty.R
import com.supersoft.coffeeloyalty.navigation.AppNavigator
import com.supersoft.coffeeloyalty.ui.main.MainActivity
import dagger.Binds
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Navigator

/**
 * Created by alex.lobur on 07.09.2019.
 * Email: lobur.a.y@gmail.com
 */
@Module
abstract class MainActivityModule {
    @Binds
    abstract fun bindActivity(activity: MainActivity): FragmentActivity

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideNavigator(activity: FragmentActivity): Navigator {
            return AppNavigator(activity, R.id.container)
        }
    }
}