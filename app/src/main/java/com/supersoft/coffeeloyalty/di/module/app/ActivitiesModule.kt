package com.supersoft.coffeeloyalty.di.module.app

import com.supersoft.coffeeloyalty.ui.main.MainActivity
import com.supersoft.coffeeloyalty.di.module.activity.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by alex.lobur on 07.09.2019.
 * Email: lobur.a.y@gmail.com
 */
@Module
interface ActivitiesModule {

    @ContributesAndroidInjector(
        modules = [
            MainActivityModule::class
        ])
    fun mainActivityInjector(): MainActivity
}