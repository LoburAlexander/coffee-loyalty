package com.supersoft.coffeeloyalty.di.module.app

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import dagger.Binds
import dagger.Module
import dagger.Provides
import java.util.Locale

/**
 * Created by alex.lobur on 07.09.2019.
 * Email: lobur.a.y@gmail.com
 */

@Module(includes = [AndroidModule.Bindings::class])
class AndroidModule {

    @Provides
    fun provideResources(context: Context): Resources =
        getLocalizedResources(context, Locale("ru", "RU"))

    private fun getLocalizedResources(
        context: Context,
        desiredLocale: Locale
    ): Resources {
        var conf: Configuration = context.resources.configuration
        conf = Configuration(conf)
        conf.setLocale(desiredLocale)
        val localizedContext = context.createConfigurationContext(conf)
        return localizedContext.resources
    }

    @Module
    interface Bindings {
        @Binds
        fun bindContext(app: Application): Context
    }
}