package com.supersoft.coffeeloyalty.di.module.app

import com.supersoft.coffeeloyalty.data.di.module.DataModule
import dagger.Module

/**
 * Created by alex.lobur on 15.09.2019.
 * Email: lobur.a.y@gmail.com
 */
@Module(includes = [
    DataModule::class
])
abstract class FacadeModule