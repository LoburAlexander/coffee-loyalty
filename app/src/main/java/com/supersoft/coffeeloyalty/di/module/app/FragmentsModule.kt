package com.supersoft.coffeeloyalty.di.module.app

import com.supersoft.coffeeloyalty.ui.activate.ActivateCardFragment
import com.supersoft.coffeeloyalty.ui.cardprocess.ProcessCardFragment
import com.supersoft.coffeeloyalty.ui.home.HomeFragment
import com.supersoft.coffeeloyalty.ui.password.PasswordFragment
import com.supersoft.coffeeloyalty.ui.settings.SettingsFragment
import com.supersoft.coffeeloyalty.ui.statistics.StatisticsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by alex.lobur on 07.09.2019.
 * Email: lobur.a.y@gmail.com
 */
@Module
interface FragmentsModule {

    @ContributesAndroidInjector
    fun homeFragmentInjector(): HomeFragment

    @ContributesAndroidInjector
    fun passwordFragmentInjector(): PasswordFragment

    @ContributesAndroidInjector
    fun settingsFragmentInjector(): SettingsFragment

    @ContributesAndroidInjector
    fun statisticsFragmentInjector(): StatisticsFragment

    @ContributesAndroidInjector
    fun activateCardFragmentInjector(): ActivateCardFragment

    @ContributesAndroidInjector
    fun processCardFragmentInjector(): ProcessCardFragment
}