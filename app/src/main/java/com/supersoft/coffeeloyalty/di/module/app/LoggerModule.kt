package com.supersoft.coffeeloyalty.di.module.app

import com.supersoft.coffeeloyalty.common.logger.AppLogger
import com.supersoft.coffeeloyalty.logger.AppLoggerImpl
import dagger.Binds
import dagger.Module

/**
 * Created by alex.lobur on 14.12.2019.
 * Email: lobur.a.y@gmail.com
 */

@Module
interface LoggerModule {

    @Binds
    fun bindAppLogger(appLoggerImpl: AppLoggerImpl): AppLogger
}