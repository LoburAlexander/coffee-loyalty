package com.supersoft.coffeeloyalty.di.module.app

import android.content.Context
import android.nfc.NfcAdapter
import com.supersoft.coffeeloyalty.domain.relays.cardconnect.CardConnectedListener
import com.supersoft.coffeeloyalty.domain.relays.cardconnect.CardConnectedProvider
import com.supersoft.coffeeloyalty.nfc.NfcCardConnectionDispatcher
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by alex.lobur on 02.10.2019.
 * Email: lobur.a.y@gmail.com
 */
@Module(includes = [NfcModule.Bindings::class])
class NfcModule {

    @Provides
    @Singleton
    fun provideNfcAdapter(appContext: Context): NfcAdapter {
        return NfcAdapter.getDefaultAdapter(appContext)
    }

    @Module
    interface Bindings {
        @Binds
        fun bindCardConnectionListener(cardConnectionDispatcher: NfcCardConnectionDispatcher): CardConnectedListener

        @Binds
        fun bindCardConnectionProvider(cardConnectionDispatcher: NfcCardConnectionDispatcher): CardConnectedProvider
    }
}