package com.supersoft.coffeeloyalty.di.module.app

import com.supersoft.coffeeloyalty.receiver.NfcTagReceiver
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by alex.lobur on 02.10.2019.
 * Email: lobur.a.y@gmail.com
 */
@Module
interface ReceiversModule {

    @ContributesAndroidInjector
    fun nfcTagReceiverInjector(): NfcTagReceiver
}