package com.supersoft.coffeeloyalty.logger

import com.supersoft.coffeeloyalty.BuildConfig
import com.supersoft.coffeeloyalty.common.logger.AppLogger
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by alex.lobur on 14.12.2019.
 * Email: lobur.a.y@gmail.com
 */
@Singleton
class AppLoggerImpl @Inject constructor() : AppLogger {

    init {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun log(message: String) {
        Timber.tag(APP_TAG)
        Timber.d(message)
    }

    override fun log(error: Throwable) {
        Timber.tag(APP_TAG)
        Timber.d(error)
    }

    override fun log(message: String, error: Throwable) {
        Timber.tag(APP_TAG)
        Timber.d(error, message)
    }

    companion object {
        private const val APP_TAG = "AppLogger"
    }
}