package com.supersoft.coffeeloyalty.navigation

import androidx.annotation.IdRes
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Forward
import javax.inject.Inject

/**
 * Created by alex.lobur on 08.09.2019.
 * Email: lobur.a.y@gmail.com
 */

class AppNavigator : SupportAppNavigator {
    private val mActivity: FragmentActivity
    @IdRes
    private val mContainerId: Int

    @Inject
    constructor(activity: FragmentActivity) : this(activity, 0)

    constructor(activity: FragmentActivity, containerId: Int) : super(activity, containerId) {
        mActivity = activity
        mContainerId = containerId
    }

    constructor(activity: FragmentActivity, fragmentManager: FragmentManager, containerId: Int) : super(activity, fragmentManager, containerId) {
        mActivity = activity
        mContainerId = containerId
    }

    override fun fragmentForward(command: Forward?) {
        val topFragment = mActivity.supportFragmentManager.findFragmentById(mContainerId)

        super.fragmentForward(command)
        mActivity.supportFragmentManager.executePendingTransactions()

        val newFragment = mActivity.supportFragmentManager.findFragmentById(mContainerId)

        if (topFragment != null) {
            newFragment?.setTargetFragment(topFragment, 0)
        }
    }

    override fun fragmentBack() {
        if (mActivity.supportFragmentManager.backStackEntryCount > 1) {
            super.fragmentBack()
        } else {
            activityBack()
        }
    }
}