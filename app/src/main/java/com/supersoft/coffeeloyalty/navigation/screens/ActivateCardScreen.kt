package com.supersoft.coffeeloyalty.navigation.screens

import androidx.fragment.app.Fragment
import com.supersoft.coffeeloyalty.ui.activate.ActivateCardFragment

/**
 * Created by alex.lobur on 16.09.2019.
 * Email: lobur.a.y@gmail.com
 */
class ActivateCardScreen : BaseScreen() {
    override val key: ScreenType = ScreenType.ACTIVATE_CARD

    override fun getFragment(): Fragment {
        return ActivateCardFragment.newInstance()
    }
}