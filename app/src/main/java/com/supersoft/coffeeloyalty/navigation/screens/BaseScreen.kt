package com.supersoft.coffeeloyalty.navigation.screens

import ru.terrakok.cicerone.android.support.SupportAppScreen
import java.io.Serializable

/**
 * Created by alex.lobur on 08.09.2019.
 * Email: lobur.a.y@gmail.com
 */
abstract class BaseScreen : SupportAppScreen(), Serializable {
    protected abstract val key: ScreenType

    final override fun getScreenKey(): String = key.toString()
}