package com.supersoft.coffeeloyalty.navigation.screens

import androidx.fragment.app.Fragment
import com.supersoft.coffeeloyalty.ui.home.HomeFragment

/**
 * Created by alex.lobur on 08.09.2019.
 * Email: lobur.a.y@gmail.com
 */
class HomeScreen : BaseScreen() {
    override val key: ScreenType = ScreenType.HOME

    override fun getFragment(): Fragment {
        return HomeFragment.newInstance()
    }
}