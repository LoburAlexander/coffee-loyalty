package com.supersoft.coffeeloyalty.navigation.screens

import androidx.fragment.app.Fragment
import com.supersoft.coffeeloyalty.ui.password.PasswordFragment

/**
 * Created by alex.lobur on 16.09.2019.
 * Email: lobur.a.y@gmail.com
 */
class PasswordScreen(
    private val targetScreen: BaseScreen
) : BaseScreen() {
    override val key: ScreenType = ScreenType.PASSWORD

    override fun getFragment(): Fragment {
        return PasswordFragment.newInstance(targetScreen)
    }
}