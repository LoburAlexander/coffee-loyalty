package com.supersoft.coffeeloyalty.navigation.screens

import androidx.fragment.app.Fragment
import com.supersoft.coffeeloyalty.ui.cardprocess.ProcessCardFragment

/**
 * Created by alex.lobur on 16.09.2019.
 * Email: lobur.a.y@gmail.com
 */
class ProcessCardScreen : BaseScreen() {
    override val key: ScreenType = ScreenType.PROCESS_CARD

    override fun getFragment(): Fragment {
        return ProcessCardFragment.newInstance()
    }
}