package com.supersoft.coffeeloyalty.navigation.screens

/**
 * Created by alex.lobur on 08.09.2019.
 * Email: lobur.a.y@gmail.com
 */
enum class ScreenType {
    HOME,
    PASSWORD,
    SETTINGS,
    STATISTICS,
    ACTIVATE_CARD,
    PROCESS_CARD
}