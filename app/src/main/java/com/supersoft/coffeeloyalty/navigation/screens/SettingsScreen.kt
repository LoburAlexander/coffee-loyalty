package com.supersoft.coffeeloyalty.navigation.screens

import androidx.fragment.app.Fragment
import com.supersoft.coffeeloyalty.ui.settings.SettingsFragment

/**
 * Created by alex.lobur on 16.09.2019.
 * Email: lobur.a.y@gmail.com
 */
class SettingsScreen : BaseScreen() {
    override val key: ScreenType = ScreenType.SETTINGS

    override fun getFragment(): Fragment {
        return SettingsFragment.newInstance()
    }
}