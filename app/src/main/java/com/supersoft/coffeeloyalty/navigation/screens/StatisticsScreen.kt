package com.supersoft.coffeeloyalty.navigation.screens

import androidx.fragment.app.Fragment
import com.supersoft.coffeeloyalty.ui.statistics.StatisticsFragment

/**
 * Created by alex.lobur on 16.09.2019.
 * Email: lobur.a.y@gmail.com
 */
class StatisticsScreen : BaseScreen() {
    override val key: ScreenType = ScreenType.STATISTICS

    override fun getFragment(): Fragment {
        return StatisticsFragment.newInstance()
    }
}