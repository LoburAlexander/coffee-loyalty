package com.supersoft.coffeeloyalty.nfc

import android.content.Intent
import android.nfc.NdefMessage
import android.nfc.NdefRecord
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.Ndef
import com.supersoft.coffeeloyalty.BuildConfig
import com.supersoft.coffeeloyalty.data.utils.NfcTagUtils
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCardPayload
import com.supersoft.coffeeloyalty.domain.error.card.base.CardWriteFailedError
import com.supersoft.coffeeloyalty.domain.error.card.base.UnsupportedCardTypeError
import io.reactivex.Completable
import io.reactivex.Single
import java.lang.Exception
import java.nio.charset.StandardCharsets
import javax.inject.Inject

/**
 * Created by alex.lobur on 05.10.2019.
 * Email: lobur.a.y@gmail.com
 */
class LoyaltyCardNfcHelper
@Inject
constructor(
    private val loyaltyCardNfcMapper: LoyaltyCardNfcMapper
) {

    fun readLoyaltyCard(tagIntent: Intent): Single<LoyaltyCardPayload> {
        return Single.defer {
            Single.fromCallable {
                val ndefMessagesParcels =
                    tagIntent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)

                val tagId = tagIntent.getByteArrayExtra(NfcAdapter.EXTRA_ID)
                val tagIdStr = tagId?.let { NfcTagUtils.tagIdBytesToString(it) }

                var loyaltyCardIdRecord: NdefRecord? = null
                ndefMessagesParcels?.forEach messages@{ ndefMessageParcel ->
                    if (ndefMessageParcel is NdefMessage) {
                        ndefMessageParcel.records?.forEach records@{ ndefRecord ->
                            if (ndefRecord.tnf == NdefRecord.TNF_EXTERNAL_TYPE) {
                                val recordType = String(ndefRecord.type, StandardCharsets.UTF_8)
                                val recordTypeElements = recordType.split(':')
                                val recordDomain = recordTypeElements.getOrNull(0)
                                val recordMimeType = recordTypeElements.getOrNull(1)

                                if (recordDomain == NFC_DOMAIN && recordMimeType == NFC_TYPE_LOYALTY_CARD_ID) {
                                    loyaltyCardIdRecord = ndefRecord
                                    return@messages
                                }
                            }
                        }
                    }
                }

                val loyaltyCardId =
                    loyaltyCardIdRecord?.let { loyaltyCardNfcMapper.mapNfcToDomain(it.payload) }

                LoyaltyCardPayload(
                    tagId = tagIdStr,
                    cardId = loyaltyCardId
                )
            }
        }
    }

    fun writeLoyaltyCard(tag: Tag, payload: LoyaltyCardPayload): Completable {
        return Completable.defer {
            Completable.fromAction {
                val ndefTag: Ndef? = Ndef.get(tag)

                if (ndefTag == null) {
                    throw UnsupportedCardTypeError()
                } else {
                    val loyaltyCardIdBytes = if (payload.cardId != null)
                        loyaltyCardNfcMapper.mapDomainToNfc(payload.cardId!!)
                    else
                        ByteArray(0)

                    val loyaltyCardRecord =
                        NdefRecord.createExternal(NFC_DOMAIN, NFC_TYPE_LOYALTY_CARD_ID, loyaltyCardIdBytes)

                    try {
                        ndefTag.connect()

                        // Clean tag
                        ndefTag.writeNdefMessage(
                            NdefMessage(
                                NdefRecord(
                                    NdefRecord.TNF_EMPTY,
                                    null,
                                    null,
                                    null
                                )
                            )
                        )
                        ndefTag.writeNdefMessage(
                            NdefMessage(loyaltyCardRecord)
                        )
                        ndefTag.close()
                    } catch (ex: Exception) {
                        throw CardWriteFailedError("Failed to write ndef card", ex)
                    }
                }
            }
        }
    }

    companion object {
        const val NFC_DOMAIN: String = BuildConfig.APPLICATION_ID
        const val NFC_TYPE_LOYALTY_CARD_ID: String = "application/loyalty+card+id"
    }
}