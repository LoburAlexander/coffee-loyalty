package com.supersoft.coffeeloyalty.nfc

import javax.inject.Inject

/**
 * Created by alex.lobur on 05.10.2019.
 * Email: lobur.a.y@gmail.com
 */
class LoyaltyCardNfcMapper
@Inject
constructor() {

    fun mapNfcToDomain(bytes: ByteArray): String{
        return String(bytes, Charsets.US_ASCII)
    }

    fun mapDomainToNfc(cardId: String): ByteArray {
        return cardId.toByteArray(Charsets.US_ASCII)
    }
}