package com.supersoft.coffeeloyalty.nfc

import android.content.Intent
import android.nfc.NfcAdapter
import android.nfc.Tag
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCardPayload
import com.supersoft.coffeeloyalty.domain.relays.cardconnect.CardConnection
import io.reactivex.Completable
import io.reactivex.Single

/**
 * Created by alex.lobur on 05.10.2019.
 * Email: lobur.a.y@gmail.com
 */
class NfcCardConnection
constructor(
    private val tagIntent: Intent,
    private val loyaltyCardNfcHelper: LoyaltyCardNfcHelper
) : CardConnection() {

    private val tag: Tag = tagIntent.getParcelableExtra(NfcAdapter.EXTRA_TAG)

    override fun readCardPayload(): Single<LoyaltyCardPayload> {
        return loyaltyCardNfcHelper.readLoyaltyCard(tagIntent)
    }

    override fun writeCardPayload(payload: LoyaltyCardPayload): Completable {
        return loyaltyCardNfcHelper.writeLoyaltyCard(tag, payload)
    }
}