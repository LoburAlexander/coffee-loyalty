package com.supersoft.coffeeloyalty.nfc

import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import com.supersoft.coffeeloyalty.domain.relays.cardconnect.CardConnectedListener
import com.supersoft.coffeeloyalty.domain.relays.cardconnect.CardConnectedProvider
import com.supersoft.coffeeloyalty.domain.relays.cardconnect.CardConnection
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by alex.lobur on 05.10.2019.
 * Email: lobur.a.y@gmail.com
 */
@Singleton
class NfcCardConnectionDispatcher
@Inject
constructor() : CardConnectedListener, CardConnectedProvider {
    private val cardConnectionRelay: Relay<CardConnection> = PublishRelay.create()

    override val cardConnectedObs: Observable<CardConnection>
        get() = cardConnectionRelay

    override fun onCardConnected(cardConnection: CardConnection) {
        cardConnectionRelay.accept(cardConnection)
    }
}