package com.supersoft.coffeeloyalty.presentation.activate

import android.content.Context
import com.arellomobile.mvp.InjectViewState
import com.supersoft.coffeeloyalty.R
import com.supersoft.coffeeloyalty.common.logger.logger
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCardPayload
import com.supersoft.coffeeloyalty.domain.error.card.CardAlreadyActivatedError
import com.supersoft.coffeeloyalty.domain.relays.cardconnect.CardConnectedProvider
import com.supersoft.coffeeloyalty.domain.usecase.card.ActivateCardUseCase
import com.supersoft.coffeeloyalty.domain.usecase.card.RemoveCardCompletelyUseCase
import com.supersoft.coffeeloyalty.presentation.base.BasePresenter
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by alex.lobur on 16.09.2019.
 * Email: lobur.a.y@gmail.com
 */
@InjectViewState
class ActivateCardPresenter
@Inject
constructor(
    private val router: Router,
    private val appContext: Context,
    private val cardConnectedProvider: CardConnectedProvider,
    private val activateCard: ActivateCardUseCase,
    private val removeCardCompletely: RemoveCardCompletelyUseCase
) : BasePresenter<ActivateCardView>() {

    private var cardActivationSub: Disposable? = null

    // -------------------------------
    //  Overridden
    // -------------------------------

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        showDefaultTextWithoutProgress()
    }

    override fun attachView(view: ActivateCardView?) {
        super.attachView(view)

        cardConnectedProvider.cardConnectedObs
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe({ cardConnection ->
                if (cardActivationSub == null || cardActivationSub!!.isDisposed) {
                    cardActivationSub = Completable.complete()
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnComplete { viewState.showTextWithProgress(appContext.getString(R.string.activate_card_progress_reading_tag)) }
                        .observeOn(Schedulers.io())
                        .andThen(
                            cardConnection.readCardPayload()
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnSuccess { viewState.showTextWithProgress(appContext.getString(R.string.activate_card_progress_activating)) }
                                .observeOn(Schedulers.io())
                                .flatMapCompletable { payload ->
                                    activateCard.execute(payload)
                                        .flatMapCompletable { loyaltyCard ->
                                            val resultPayload = LoyaltyCardPayload(
                                                tagId = payload.tagId,
                                                cardId = loyaltyCard.id
                                            )
                                            cardConnection.writeCardPayload(resultPayload)
                                                .onErrorResumeNext { error ->
                                                    removeCardCompletely.execute(loyaltyCard.id)
                                                        .andThen(Completable.error(error))
                                                }
                                        }
                                }
                        )
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            showDefaultTextWithoutProgress()
                            viewState.showMessage(appContext.getString(R.string.activate_card_success))
                            router.exit()
                        }, { error ->
                            showDefaultTextWithoutProgress()

                            when (error) {
                                is CardAlreadyActivatedError -> {
                                    viewState.showMessage(appContext.getString(R.string.activate_card_already_activated))
                                }
                                else -> {
                                    viewState.showMessage(appContext.getString(R.string.activate_card_error_general))
                                }
                            }
                            logger.log(error)
                        })
                }
            }, { error ->
                logger.log(error)
            })
            .untilDetach()
    }

    override fun detachView(view: ActivateCardView) {
        super.detachView(view)
        cardActivationSub?.dispose()
    }

    override fun onBackPressed() {
        router.exit()
    }

    // -------------------------------
    //  Public interface
    // -------------------------------

    // -------------------------------
    //  Private
    // -------------------------------

    private fun showDefaultTextWithoutProgress() {
        viewState.showTextWithoutProgress(appContext.getString(R.string.activate_card_progress_scan_tag))
    }
}