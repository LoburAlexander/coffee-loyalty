package com.supersoft.coffeeloyalty.presentation.activate

import com.arellomobile.mvp.MvpView

/**
 * Created by alex.lobur on 16.09.2019.
 * Email: lobur.a.y@gmail.com
 */
interface ActivateCardView : MvpView {
    fun showTextWithProgress(text: String)
    fun showTextWithoutProgress(text: String)
    fun hideProgress()
    fun showMessage(message: String)
}