package com.supersoft.coffeeloyalty.presentation.base

/**
 * Created by alex.lobur on 02.10.2019.
 * Email: lobur.a.y@gmail.com
 */
interface BackPressedListener {
    fun onBackPressed()
}