package com.supersoft.coffeeloyalty.presentation.base

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by alex.lobur on 16.09.2019.
 * Email: lobur.a.y@gmail.com
 */
abstract class BasePresenter<ViewT : MvpView> : MvpPresenter<ViewT>(), BackPressedListener {
    private val destroyDisposable: CompositeDisposable = CompositeDisposable()
    private val detachDisposable: CompositeDisposable = CompositeDisposable()

    // -------------------------------
    //  Overridden
    // -------------------------------

    override fun detachView(view: ViewT) {
        super.detachView(view)
        detachDisposable.clear()
    }

    override fun onDestroy() {
        destroyDisposable.clear()
        super.onDestroy()
    }

    override fun onBackPressed() = Unit

    // -------------------------------
    //  Protected
    // -------------------------------

    protected fun Disposable.untilDetach() {
        detachDisposable.add(this)
    }

    protected fun Disposable.untilDestroy() {
        destroyDisposable.add(this)
    }
}