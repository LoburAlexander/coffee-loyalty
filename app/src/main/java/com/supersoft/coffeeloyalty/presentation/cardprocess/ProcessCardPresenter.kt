package com.supersoft.coffeeloyalty.presentation.cardprocess

import android.content.res.Resources
import android.text.Html
import com.arellomobile.mvp.InjectViewState
import com.supersoft.coffeeloyalty.R
import com.supersoft.coffeeloyalty.common.logger.logger
import com.supersoft.coffeeloyalty.domain.error.card.CardNotActivatedError
import com.supersoft.coffeeloyalty.domain.relays.cardconnect.CardConnectedProvider
import com.supersoft.coffeeloyalty.domain.repository.AppSettingsRepository
import com.supersoft.coffeeloyalty.domain.usecase.card.PerformCardActionUseCase
import com.supersoft.coffeeloyalty.presentation.base.BasePresenter
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import ru.terrakok.cicerone.Router
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by alex.lobur on 05.10.2019.
 * Email: lobur.a.y@gmail.com
 */
@InjectViewState
class ProcessCardPresenter
@Inject
constructor(
    private val router: Router,
    private val resources: Resources,
    private val cardConnectedProvider: CardConnectedProvider,
    private val performCardAction: PerformCardActionUseCase,
    appSettingsRepository: AppSettingsRepository
) : BasePresenter<ProcessCardView>() {

    private val isProcessCardAllowedSubject = BehaviorSubject.createDefault(true)
    private val cardProcessDelaySeconds = appSettingsRepository.cardProcessDelaySeconds.toLong()

    private var isProcessCardAllowed: Boolean
        set(value) = isProcessCardAllowedSubject.onNext(value)
        get() = isProcessCardAllowedSubject.value!!

    private var isViewAnimating: Boolean = false
    private var cardProcessingSub: Disposable? = null

    // -------------------------------
    //  Overridden
    // -------------------------------

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        showDefaultTextWithoutProgress()
    }

    override fun attachView(view: ProcessCardView?) {
        super.attachView(view)

        cardConnectedProvider.cardConnectedObs
            .observeOn(AndroidSchedulers.mainThread())
            .filter {
                val canProcessCard = !isViewAnimating && isProcessCardAllowed

                if (!isProcessCardAllowed) {
                    viewState.showMessage(resources.getString(R.string.process_card_info_process_delay, cardProcessDelaySeconds))
                }

                canProcessCard
            }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe({ cardConnection ->
                if (cardProcessingSub == null || cardProcessingSub!!.isDisposed) {
                    cardProcessingSub = Completable.complete()
                        .observeOn(AndroidSchedulers.mainThread())
//                        .doOnComplete { viewState.showTextWithProgress(resources.getString(R.string.process_card_progress_reading_tag)) }
                        .observeOn(Schedulers.io())
                        .andThen(
                            cardConnection.readCardPayload()
                                .observeOn(AndroidSchedulers.mainThread())
//                                .doOnSuccess { viewState.showTextWithProgress(resources.getString(R.string.process_card_progress_process_tag)) }
                                .observeOn(Schedulers.io())
                                .flatMap { payload ->
                                    performCardAction.execute(payload)
                                }
                        )
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ remainingActions ->
                            showDefaultTextWithoutProgress()

                            if (remainingActions == 0) {
                                viewState.showRewardAnimation()
                                waitViewAnimation(ANIMATION_REWARD_MILLIS)
                            } else {
                                val htmlString = resources.getQuantityString(R.plurals.process_card_actions_left, remainingActions, remainingActions)
                                @Suppress("DEPRECATION")
                                val resultString = Html.fromHtml(htmlString)

                                viewState.showActionsLeftAnimation(resultString)
                                waitViewAnimation(ANIMATION_ACTIONS_LEFT_MILLIS)
                            }

                            waitProcessCardDelay()
                        }, { error ->
                            showDefaultTextWithoutProgress()

                            if (error is CardNotActivatedError) {
                                viewState.showMessage(resources.getString(R.string.process_card_error_not_activated))
                            } else {
                                viewState.showMessage(resources.getString(R.string.process_card_error_general))
                            }

                            logger.log(error)
                        })
                }
            }, { error ->
                logger.log(error)
            })
            .untilDetach()
    }

    override fun detachView(view: ProcessCardView) {
        super.detachView(view)
        cardProcessingSub?.dispose()
    }

    override fun onBackPressed() {
        router.exit()
    }

    // -------------------------------
    //  Public interface
    // -------------------------------

    // -------------------------------
    //  Private
    // -------------------------------

    private fun waitProcessCardDelay() {
        isProcessCardAllowed = false
        Completable.timer(cardProcessDelaySeconds, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .subscribe {
                isProcessCardAllowed = true
            }
            .untilDestroy()
    }

    private fun waitViewAnimation(durationMillis: Long) {
        isViewAnimating = true
        Completable.timer(durationMillis, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .subscribe {
                isViewAnimating = false
            }
            .untilDestroy()
    }

    private fun showDefaultTextWithoutProgress() {
        viewState.showTextWithoutProgress(resources.getString(R.string.process_card_progress_scan_tag))
    }

    companion object {
        const val ANIMATION_ACTIONS_LEFT_IN_OUT_MILLIS = 500L
        const val ANIMATION_ACTIONS_LEFT_REST_MILLIS = 1000L
        const val ANIMATION_ACTIONS_LEFT_MILLIS = 2 * ANIMATION_ACTIONS_LEFT_IN_OUT_MILLIS + ANIMATION_ACTIONS_LEFT_REST_MILLIS
        const val ANIMATION_REWARD_IN_OUT_MILLIS = 600L
        const val ANIMATION_REWARD_REST_MILLIS = 1400L
        const val ANIMATION_REWARD_MILLIS = 2 * ANIMATION_REWARD_IN_OUT_MILLIS + ANIMATION_REWARD_REST_MILLIS
    }
}