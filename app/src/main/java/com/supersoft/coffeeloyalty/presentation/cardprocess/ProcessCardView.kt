package com.supersoft.coffeeloyalty.presentation.cardprocess

import com.arellomobile.mvp.MvpView

/**
 * Created by alex.lobur on 05.10.2019.
 * Email: lobur.a.y@gmail.com
 */
interface ProcessCardView : MvpView {

    fun showTextWithProgress(text: String)

    fun showTextWithoutProgress(text: String)

    fun showActionsLeftAnimation(actionsLeftText: CharSequence)

    fun showRewardAnimation()

    fun hideProgress()

    fun showMessage(message: String)
}