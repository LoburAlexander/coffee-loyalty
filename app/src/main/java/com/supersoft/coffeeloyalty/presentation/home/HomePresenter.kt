package com.supersoft.coffeeloyalty.presentation.home

import com.arellomobile.mvp.InjectViewState
import com.supersoft.coffeeloyalty.navigation.screens.ActivateCardScreen
import com.supersoft.coffeeloyalty.navigation.screens.BaseScreen
import com.supersoft.coffeeloyalty.navigation.screens.PasswordScreen
import com.supersoft.coffeeloyalty.navigation.screens.ProcessCardScreen
import com.supersoft.coffeeloyalty.navigation.screens.SettingsScreen
import com.supersoft.coffeeloyalty.navigation.screens.StatisticsScreen
import com.supersoft.coffeeloyalty.presentation.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by alex.lobur on 08.09.2019.
 * Email: lobur.a.y@gmail.com
 */
@InjectViewState
class HomePresenter
@Inject
constructor(
    private val router: Router
) : BasePresenter<HomeView>() {

    // -------------------------------
    //  Overridden
    // -------------------------------

    override fun onBackPressed() {
        router.exit()
    }

    // -------------------------------
    //  Public interface
    // -------------------------------

    fun onStatisticsMenuClick() {
        openSecuredScreen(StatisticsScreen())
    }

    fun onSettingsMenuClick() {
        openSecuredScreen(SettingsScreen())
    }

    fun onProcessCardsModeClick() {
        openSecuredScreen(ProcessCardScreen())
    }

    fun onActivateCardClick() {
        router.navigateTo(ActivateCardScreen())
    }

    fun onResetCardClick() {

    }

    // -------------------------------
    //  Private
    // -------------------------------

    private fun openSecuredScreen(targetScreen: BaseScreen) {
        router.navigateTo(PasswordScreen(targetScreen))
    }
}