package com.supersoft.coffeeloyalty.presentation.main

import com.arellomobile.mvp.InjectViewState
import com.supersoft.coffeeloyalty.navigation.screens.HomeScreen
import com.supersoft.coffeeloyalty.presentation.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by alex.lobur on 07.09.2019.
 * Email: lobur.a.y@gmail.com
 */

@InjectViewState
class MainPresenter
@Inject
constructor(
    private val router: Router
) : BasePresenter<MainView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        router.navigateTo(HomeScreen())
    }
}