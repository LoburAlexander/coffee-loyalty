package com.supersoft.coffeeloyalty.presentation.password

import com.arellomobile.mvp.InjectViewState
import com.supersoft.coffeeloyalty.common.utils.empty
import com.supersoft.coffeeloyalty.domain.usecase.password.ValidateScreenPasswordUseCase
import com.supersoft.coffeeloyalty.navigation.screens.BaseScreen
import com.supersoft.coffeeloyalty.presentation.base.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import ru.terrakok.cicerone.Router
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by alex.lobur on 15.12.2019.
 * Email: lobur.a.y@gmail.com
 */

@InjectViewState
class PasswordPresenter
@Inject
constructor(
    private val router: Router,
    private val validateScreenPassword: ValidateScreenPasswordUseCase
) : BasePresenter<PasswordView>() {

    private val currentPasswordTextSubject = BehaviorSubject.createDefault(String.empty())

    private var currentPasswordText: String
        set(value) = currentPasswordTextSubject.onNext(value)
        get() = currentPasswordTextSubject.value!!

    private lateinit var targetScreen: BaseScreen

    // -------------------------------
    //  Overridden
    // -------------------------------

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        currentPasswordTextSubject
            .debounce(DEBOUNCE_VALIDATE_PASSWORD_MILLIS, TimeUnit.MILLISECONDS)
            .flatMapSingle(validateScreenPassword::execute)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { isPasswordCorrect ->
                if (isPasswordCorrect) {
                    router.replaceScreen(targetScreen)
                }
            }
            .untilDestroy()
    }

    override fun attachView(view: PasswordView?) {
        super.attachView(view)

        viewState.clearPasswordText()
        viewState.focusOnPasswordTextInput()
    }

    override fun onBackPressed() {
        closeScreen()
    }

    // -------------------------------
    //  Public interface
    // -------------------------------

    fun setTargetScreen(targetScreen: BaseScreen) {
        this.targetScreen = targetScreen
    }

    fun onCloseClick() {
        closeScreen()
    }

    fun onPasswordTextChanged(passwordText: String) {
        currentPasswordText = passwordText
    }

    // -------------------------------
    //  Private
    // -------------------------------

    private fun closeScreen() {
        router.exit()
    }

    companion object {
        private const val DEBOUNCE_VALIDATE_PASSWORD_MILLIS = 200L
    }
}