package com.supersoft.coffeeloyalty.presentation.password

import com.arellomobile.mvp.MvpView

/**
 * Created by alex.lobur on 07.09.2019.
 * Email: lobur.a.y@gmail.com
 */
interface PasswordView : MvpView {

    fun clearPasswordText()

    fun focusOnPasswordTextInput()
}