package com.supersoft.coffeeloyalty.presentation.settings

import android.content.res.Resources
import com.arellomobile.mvp.InjectViewState
import com.supersoft.coffeeloyalty.R
import com.supersoft.coffeeloyalty.domain.repository.AppSettingsRepository
import com.supersoft.coffeeloyalty.presentation.base.BasePresenter
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import ru.terrakok.cicerone.Router
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by alex.lobur on 15.12.2019.
 * Email: lobur.a.y@gmail.com
 */

@InjectViewState
class SettingsPresenter
@Inject
constructor(
    private val router: Router,
    private val resources: Resources,
    private val appSettingsRepository: AppSettingsRepository
) : BasePresenter<SettingsView>() {

    private val currentActionCountForRewardValueSubject = BehaviorSubject.create<Int>()
    private val currentCardProcessDelayValueSubject = BehaviorSubject.create<Int>()

    private var currentActionCountForRewardValue: Int
        set(value) = currentActionCountForRewardValueSubject.onNext(value)
        get() = currentActionCountForRewardValueSubject.value!!
    private var currentCardProcessDelayValue: Int
        set(value) = currentCardProcessDelayValueSubject.onNext(value)
        get() = currentCardProcessDelayValueSubject.value!!

    // -------------------------------
    //  Overridden
    // -------------------------------

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        currentActionCountForRewardValue = appSettingsRepository.actionCountForReward
        currentCardProcessDelayValue = appSettingsRepository.cardProcessDelaySeconds

        updateActionCountForRewardValue()
        updateCardProcessDelayValue()

        saveAppSettingsWithDebounce(
            settingValueSubject = currentActionCountForRewardValueSubject,
            saveSettingObsBuilder = { value ->
                Completable.fromAction {
                    appSettingsRepository.actionCountForReward = value
                }
            }
        )

        saveAppSettingsWithDebounce(
            settingValueSubject = currentCardProcessDelayValueSubject,
            saveSettingObsBuilder = { value ->
                Completable.fromAction {
                    appSettingsRepository.cardProcessDelaySeconds = value
                }
            }
        )
    }

    override fun onBackPressed() {
        router.exit()
    }

    // -------------------------------
    //  Public interface
    // -------------------------------

    fun onActionCountForRewardValueChanged(value: Int) {
        currentActionCountForRewardValue = value
        updateActionCountForRewardValue()
    }

    fun onCardProcessDelayValueChanged(value: Int) {
        currentCardProcessDelayValue = value
        updateCardProcessDelayValue()
    }

    // -------------------------------
    //  Private
    // -------------------------------

    private fun <TSetting> saveAppSettingsWithDebounce(
        settingValueSubject: Subject<TSetting>,
        saveSettingObsBuilder: (TSetting) -> Completable
    ) {
        settingValueSubject
            .debounce(DEBOUNCE_SAVE_PREFERENCES_MILLIS, TimeUnit.MILLISECONDS)
            .flatMapCompletable(saveSettingObsBuilder::invoke)
            .subscribeOn(Schedulers.io())
            .subscribe()
            .untilDestroy()
    }

    private fun updateActionCountForRewardValue() {
        currentActionCountForRewardValue.let { value ->
            val text = resources.getString(R.string.settings_action_count_for_reward, value)
            viewState.setActionCountForRewardText(text)
            viewState.setActionCountForRewardValue(value)
        }
    }

    private fun updateCardProcessDelayValue() {
        currentCardProcessDelayValue.let { value ->
            val text = resources.getString(R.string.settings_card_process_delay_seconds, value)
            viewState.setCardProcessDelayText(text)
            viewState.setCardProcessDelayValue(value)
        }
    }

    companion object {
        private const val DEBOUNCE_SAVE_PREFERENCES_MILLIS = 300L
    }
}