package com.supersoft.coffeeloyalty.presentation.settings

import com.arellomobile.mvp.MvpView

/**
 * Created by alex.lobur on 07.09.2019.
 * Email: lobur.a.y@gmail.com
 */
interface SettingsView : MvpView {

    fun setActionCountForRewardValue(value: Int)

    fun setCardProcessDelayValue(value: Int)

    fun setActionCountForRewardText(text: String)

    fun setCardProcessDelayText(text: String)
}