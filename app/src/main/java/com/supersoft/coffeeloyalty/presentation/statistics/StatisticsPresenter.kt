package com.supersoft.coffeeloyalty.presentation.statistics

import com.arellomobile.mvp.InjectViewState
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCardEventType
import com.supersoft.coffeeloyalty.domain.repository.LoyaltyCardEventRepository
import com.supersoft.coffeeloyalty.presentation.base.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by alex.lobur on 15.12.2019.
 * Email: lobur.a.y@gmail.com
 */

@InjectViewState
class StatisticsPresenter
@Inject
constructor(
    private val router: Router,
    private val loyaltyCardEventRepository: LoyaltyCardEventRepository
) : BasePresenter<StatisticsView>() {

    // -------------------------------
    //  Overridden
    // -------------------------------

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        setEventsCountInfo(
            eventType = LoyaltyCardEventType.ACTIVATED,
            onEventCount = { activatedCardsCount ->
                viewState.setActivatedCardsCountText(activatedCardsCount.toString())
            }
        )

        setEventsCountInfo(
            eventType = LoyaltyCardEventType.ACTION_PERFORMED,
            onEventCount = { actionsCount ->
                viewState.setActionsCountText(actionsCount.toString())
            }
        )

        setEventsCountInfo(
            eventType = LoyaltyCardEventType.REWARD_RECEIVED,
            onEventCount = { rewardReceivedCount ->
                viewState.setRewardReceivedCountText(rewardReceivedCount.toString())
            }
        )
    }

    override fun onBackPressed() {
        router.exit()
    }

    // -------------------------------
    //  Public interface
    // -------------------------------

    // -------------------------------
    //  Private
    // -------------------------------

    private fun setEventsCountInfo(eventType: LoyaltyCardEventType, onEventCount: (Int) -> Unit) {
        loyaltyCardEventRepository.getEventsCount(eventType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onEventCount::invoke)
            .untilDestroy()
    }
}