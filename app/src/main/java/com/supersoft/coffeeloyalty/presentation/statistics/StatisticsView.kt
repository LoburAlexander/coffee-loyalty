package com.supersoft.coffeeloyalty.presentation.statistics

import com.arellomobile.mvp.MvpView

/**
 * Created by alex.lobur on 07.09.2019.
 * Email: lobur.a.y@gmail.com
 */
interface StatisticsView : MvpView {

    fun setActivatedCardsCountText(text: String)

    fun setActionsCountText(text: String)

    fun setRewardReceivedCountText(text: String)
}