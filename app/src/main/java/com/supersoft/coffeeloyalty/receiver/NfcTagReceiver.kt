package com.supersoft.coffeeloyalty.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.supersoft.coffeeloyalty.domain.relays.cardconnect.CardConnectedListener
import com.supersoft.coffeeloyalty.nfc.LoyaltyCardNfcHelper
import com.supersoft.coffeeloyalty.nfc.NfcCardConnection
import dagger.android.AndroidInjection
import javax.inject.Inject

/**
 * Created by alex.lobur on 08.09.2019.
 * Email: lobur.a.y@gmail.com
 */
class NfcTagReceiver : BroadcastReceiver() {
    @Inject
    lateinit var loyaltyCardNfcHelper: LoyaltyCardNfcHelper
    @Inject
    lateinit var cardConnectedListener: CardConnectedListener

    override fun onReceive(context: Context, intent: Intent) {
        AndroidInjection.inject(this, context)

        val cardConnection = NfcCardConnection(intent, loyaltyCardNfcHelper)
        cardConnectedListener.onCardConnected(cardConnection)
    }
}