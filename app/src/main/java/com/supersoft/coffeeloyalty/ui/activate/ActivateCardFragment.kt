package com.supersoft.coffeeloyalty.ui.activate

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.supersoft.coffeeloyalty.R
import com.supersoft.coffeeloyalty.presentation.activate.ActivateCardPresenter
import com.supersoft.coffeeloyalty.presentation.activate.ActivateCardView
import com.supersoft.coffeeloyalty.ui.base.BaseFragment
import com.supersoft.coffeeloyalty.ui.utils.makeGone
import com.supersoft.coffeeloyalty.ui.utils.makeVisible
import com.supersoft.coffeeloyalty.ui.utils.showSnackbar
import kotlinx.android.synthetic.main.fragment_activate_card.vLlProgress
import kotlinx.android.synthetic.main.fragment_activate_card.vProgressBar
import kotlinx.android.synthetic.main.fragment_activate_card.vProgressImage
import kotlinx.android.synthetic.main.fragment_activate_card.vToolbar
import kotlinx.android.synthetic.main.fragment_activate_card.vTvProgressText

class ActivateCardFragment : BaseFragment<ActivateCardPresenter>(), ActivateCardView {

    override val layoutId: Int = R.layout.fragment_activate_card

    @InjectPresenter
    lateinit var presenter: ActivateCardPresenter
    @ProvidePresenter
    fun providePresenter(): ActivateCardPresenter = presenterProvider.get()

    // -------------------------------
    //  Overridden (BaseFragment)
    // -------------------------------

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    // -------------------------------
    //  Overridden (ActivateCardView)
    // -------------------------------

    override fun showTextWithProgress(text: String) {
        vTvProgressText.text = text
        vLlProgress.makeVisible()
        vProgressBar.makeVisible()
        vProgressImage.makeGone()
    }

    override fun showTextWithoutProgress(text: String) {
        vTvProgressText.text = text
        vLlProgress.makeVisible()
        vProgressBar.makeGone()
        vProgressImage.makeVisible()
    }

    override fun hideProgress() {
        vLlProgress.visibility = View.GONE
    }

    override fun showMessage(message: String) {
        showSnackbar(message)
    }

    // -------------------------------
    //  Private
    // -------------------------------

    private fun initView() {
        initToolbar(vToolbar)
    }

    private fun initToolbar(toolbar: Toolbar) {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener {
            presenter.onBackPressed()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = ActivateCardFragment()
    }
}
