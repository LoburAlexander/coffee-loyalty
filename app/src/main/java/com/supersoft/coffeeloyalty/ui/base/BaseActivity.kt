package com.supersoft.coffeeloyalty.ui.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import javax.inject.Inject
import javax.inject.Provider

/**
 * Created by alex.lobur on 08.09.2019.
 * Email: lobur.a.y@gmail.com
 */
abstract class BaseActivity<PresenterT : MvpPresenter<out MvpView>> : MvpAppCompatActivity(), HasAndroidInjector {
    @Inject
    lateinit var presenterProvider: Provider<PresenterT>
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>
    @Inject
    lateinit var navigatorHolder: NavigatorHolder
    @Inject
    lateinit var navigator: Navigator

    protected abstract val layoutId: Int @LayoutRes get

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        setContentView(layoutId)
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }
}