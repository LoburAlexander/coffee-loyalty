package com.supersoft.coffeeloyalty.ui.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.annotation.LayoutRes
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.MvpView
import com.supersoft.coffeeloyalty.presentation.base.BasePresenter
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject
import javax.inject.Provider

/**
 * Created by alex.lobur on 08.09.2019.
 * Email: lobur.a.y@gmail.com
 */
abstract class BaseFragment<PresenterT : BasePresenter<out MvpView>> : MvpAppCompatFragment() {
    @Inject
    lateinit var presenterProvider: Provider<PresenterT>

    protected abstract val layoutId: Int @LayoutRes get

    private val backPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            onBackPressed()
        }
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
        activity!!.onBackPressedDispatcher.addCallback(backPressedCallback)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId, container, false)
    }

    override fun onDetach() {
        backPressedCallback.remove()
        super.onDetach()
    }

    protected abstract fun onBackPressed()
}