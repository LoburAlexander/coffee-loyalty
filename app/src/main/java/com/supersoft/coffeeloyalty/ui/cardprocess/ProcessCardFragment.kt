package com.supersoft.coffeeloyalty.ui.cardprocess

import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationSet
import androidx.appcompat.widget.Toolbar
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.supersoft.coffeeloyalty.R
import com.supersoft.coffeeloyalty.presentation.cardprocess.ProcessCardPresenter
import com.supersoft.coffeeloyalty.presentation.cardprocess.ProcessCardView
import com.supersoft.coffeeloyalty.ui.base.BaseFragment
import com.supersoft.coffeeloyalty.ui.utils.animation.AnimationUtils
import com.supersoft.coffeeloyalty.ui.utils.animation.onAnimationEnd
import com.supersoft.coffeeloyalty.ui.utils.makeGone
import com.supersoft.coffeeloyalty.ui.utils.makeInvisible
import com.supersoft.coffeeloyalty.ui.utils.makeVisible
import com.supersoft.coffeeloyalty.ui.utils.showSnackbar
import kotlinx.android.synthetic.main.fragment_process_card.vIvFreeCoffee
import kotlinx.android.synthetic.main.fragment_process_card.vLlProgress
import kotlinx.android.synthetic.main.fragment_process_card.vProgressBar
import kotlinx.android.synthetic.main.fragment_process_card.vProgressImage
import kotlinx.android.synthetic.main.fragment_process_card.vToolbar
import kotlinx.android.synthetic.main.fragment_process_card.vTvActionsLeft
import kotlinx.android.synthetic.main.fragment_process_card.vTvFree
import kotlinx.android.synthetic.main.fragment_process_card.vTvProgressText

class ProcessCardFragment : BaseFragment<ProcessCardPresenter>(), ProcessCardView {

    override val layoutId: Int = R.layout.fragment_process_card

    @InjectPresenter
    lateinit var presenter: ProcessCardPresenter
    @ProvidePresenter
    fun providePresenter(): ProcessCardPresenter = presenterProvider.get()

    private var defaultTextInAnimation: Animation? = null
    private var defaultTextOutAnimation: Animation? = null
    private var actionsLeftTextInAnimation: Animation? = null
    private var actionsLeftTextRestAnimation: Animation? = null
    private var actionsLeftTextOutAnimation: Animation? = null
    private var rewardInAnimation: Animation? = null
    private var rewardRestAnimation: Animation? = null
    private var rewardOutAnimation: Animation? = null


    // -------------------------------
    //  Overridden (BaseFragment)
    // -------------------------------

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    // -------------------------------
    //  Overridden (ActivateCardView)
    // -------------------------------

    override fun showTextWithProgress(text: String) {
        cancelAllAnimations()

        vTvProgressText.text = text
        vLlProgress.makeVisible()
        vProgressBar.makeVisible()
        vProgressImage.makeGone()
    }

    override fun showTextWithoutProgress(text: String) {
        cancelAllAnimations()

        vTvProgressText.text = text
        vLlProgress.makeVisible()
        vProgressBar.makeGone()
        vProgressImage.makeVisible()
    }

    override fun showActionsLeftAnimation(actionsLeftText: CharSequence) {
        cancelAllAnimations()

        vTvActionsLeft.text = actionsLeftText

        vLlProgress.makeVisible()
        vTvActionsLeft.makeVisible()

        defaultTextOutAnimation = AnimationUtils.getFadeOutAnimation(ProcessCardPresenter.ANIMATION_ACTIONS_LEFT_IN_OUT_MILLIS)
        defaultTextInAnimation = AnimationUtils.getFadeInAnimation(ProcessCardPresenter.ANIMATION_ACTIONS_LEFT_IN_OUT_MILLIS)

        actionsLeftTextInAnimation = AnimationUtils.getFadeInAnimation(ProcessCardPresenter.ANIMATION_ACTIONS_LEFT_IN_OUT_MILLIS).apply {
            onAnimationEnd {
                vLlProgress.makeInvisible()
                vTvActionsLeft.startAnimation(actionsLeftTextRestAnimation)
            }
        }

        actionsLeftTextRestAnimation = AnimationUtils.getRestAnimation(ProcessCardPresenter.ANIMATION_ACTIONS_LEFT_REST_MILLIS).apply {
            onAnimationEnd {
                vTvActionsLeft.startAnimation(actionsLeftTextOutAnimation)

                vLlProgress.makeVisible()
                vLlProgress.startAnimation(defaultTextInAnimation)
            }
        }

        actionsLeftTextOutAnimation = AnimationUtils.getFadeOutAnimation(ProcessCardPresenter.ANIMATION_ACTIONS_LEFT_IN_OUT_MILLIS).apply {
            onAnimationEnd {
                vTvActionsLeft.makeInvisible()
            }
        }

        vLlProgress.startAnimation(defaultTextOutAnimation)
        vTvActionsLeft.startAnimation(actionsLeftTextInAnimation)
    }

    override fun showRewardAnimation() {
        cancelAllAnimations()

        vLlProgress.makeVisible()
        vIvFreeCoffee.makeVisible()
        vTvFree.makeVisible()

        defaultTextOutAnimation = AnimationUtils.getFadeOutAnimation(ProcessCardPresenter.ANIMATION_REWARD_IN_OUT_MILLIS, speedFactor = 0.01f)
        defaultTextInAnimation = AnimationUtils.getFadeInAnimation(ProcessCardPresenter.ANIMATION_REWARD_IN_OUT_MILLIS)

        rewardInAnimation = AnimationSet(false).apply {
            addAnimation(AnimationUtils.getFadeInAnimation(ProcessCardPresenter.ANIMATION_REWARD_IN_OUT_MILLIS, speedFactor = 2f))
            addAnimation(AnimationUtils.getScaleInAnimation(ProcessCardPresenter.ANIMATION_REWARD_IN_OUT_MILLIS))
            onAnimationEnd {
                vLlProgress.makeInvisible()
                vIvFreeCoffee.startAnimation(rewardRestAnimation)
                vTvFree.startAnimation(rewardRestAnimation)
            }
        }

        rewardRestAnimation = AnimationUtils.getRestAnimation(ProcessCardPresenter.ANIMATION_REWARD_REST_MILLIS).apply {
            onAnimationEnd {
                vIvFreeCoffee.startAnimation(rewardOutAnimation)
                vTvFree.startAnimation(rewardOutAnimation)

                vLlProgress.makeVisible()
                vLlProgress.startAnimation(defaultTextInAnimation)
            }
        }

        rewardOutAnimation = AnimationUtils.getFadeOutAnimation(ProcessCardPresenter.ANIMATION_REWARD_IN_OUT_MILLIS).apply {
            onAnimationEnd {
                vIvFreeCoffee.makeInvisible()
                vTvFree.makeInvisible()
            }
        }

        vLlProgress.startAnimation(defaultTextOutAnimation)
        vIvFreeCoffee.startAnimation(rewardInAnimation)
        vTvFree.startAnimation(rewardInAnimation)
    }

    override fun hideProgress() {
        vLlProgress.visibility = View.GONE
    }

    override fun showMessage(message: String) {
        showSnackbar(message)
    }

    // -------------------------------
    //  Private
    // -------------------------------

    private fun initView() {
        initToolbar(vToolbar)
    }

    private fun initToolbar(toolbar: Toolbar) {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener {
            presenter.onBackPressed()
        }
    }

    private fun cancelAllAnimations() {
        defaultTextInAnimation?.cancel()
        defaultTextOutAnimation?.cancel()

        actionsLeftTextInAnimation?.cancel()
        actionsLeftTextRestAnimation?.cancel()
        actionsLeftTextOutAnimation?.cancel()

        rewardInAnimation?.cancel()
        rewardRestAnimation?.cancel()
        rewardOutAnimation?.cancel()
    }

    companion object {
        @JvmStatic
        fun newInstance() = ProcessCardFragment()
    }
}
