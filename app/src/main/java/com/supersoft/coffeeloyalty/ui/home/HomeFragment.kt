package com.supersoft.coffeeloyalty.ui.home

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.supersoft.coffeeloyalty.R
import com.supersoft.coffeeloyalty.presentation.home.HomePresenter
import com.supersoft.coffeeloyalty.presentation.home.HomeView
import com.supersoft.coffeeloyalty.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment<HomePresenter>(), HomeView {

    override val layoutId: Int = R.layout.fragment_home

    @InjectPresenter
    lateinit var presenter: HomePresenter
    @ProvidePresenter
    fun providePresenter(): HomePresenter = presenterProvider.get()

    // -------------------------------
    //  Overridden (BaseFragment)
    // -------------------------------

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    // -------------------------------
    //  Private
    // -------------------------------

    private fun initView() {
        initListeners()
        initToolbar(vToolbar)
    }

    private fun initListeners() {
        vLlActionProcessCardsMode.setOnClickListener { presenter.onProcessCardsModeClick() }
        vLlActionActivateCard.setOnClickListener { presenter.onActivateCardClick() }
        vLlActionResetCard.setOnClickListener { presenter.onResetCardClick() }
    }

    private fun initToolbar(toolbar: Toolbar) {
        toolbar.inflateMenu(R.menu.menu_home)
        toolbar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.item_settings -> {
                    presenter.onSettingsMenuClick()
                    true
                }
                R.id.item_statistics -> {
                    presenter.onStatisticsMenuClick()
                    true
                }
                else -> false
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
    }
}
