package com.supersoft.coffeeloyalty.ui.main

import android.app.PendingIntent
import android.content.Intent
import android.nfc.NfcAdapter
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.supersoft.coffeeloyalty.R
import com.supersoft.coffeeloyalty.presentation.main.MainPresenter
import com.supersoft.coffeeloyalty.presentation.main.MainView
import com.supersoft.coffeeloyalty.receiver.NfcTagReceiver
import com.supersoft.coffeeloyalty.ui.base.BaseActivity
import javax.inject.Inject

class MainActivity : BaseActivity<MainPresenter>(), MainView {

    override val layoutId: Int =  R.layout.main_activity

    @InjectPresenter
    lateinit var presenter: MainPresenter
    @ProvidePresenter
    fun providePresenter(): MainPresenter = presenterProvider.get()

    @Inject
    lateinit var nfcAdapter: NfcAdapter

    // -------------------------------
    //  Overridden (Activity)
    // -------------------------------

    public override fun onResume() {
        super.onResume()
        nfcAdapter.enableForegroundDispatch(this, getNfcTagDiscoveredIntent(), arrayOf(), arrayOf())
    }

    public override fun onPause() {
        super.onPause()
        nfcAdapter.disableForegroundDispatch(this)
    }

    // TODO: Move from activity
    private fun getNfcTagDiscoveredIntent(): PendingIntent {
        val intent = Intent(this, NfcTagReceiver::class.java)
        return PendingIntent.getBroadcast(this, 0, intent, 0)
    }
}
