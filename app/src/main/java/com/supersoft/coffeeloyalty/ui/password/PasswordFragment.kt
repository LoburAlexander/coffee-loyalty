package com.supersoft.coffeeloyalty.ui.password

import android.os.Bundle
import android.view.View
import androidx.core.widget.addTextChangedListener
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.supersoft.coffeeloyalty.R
import com.supersoft.coffeeloyalty.navigation.screens.BaseScreen
import com.supersoft.coffeeloyalty.presentation.password.PasswordPresenter
import com.supersoft.coffeeloyalty.presentation.password.PasswordView
import com.supersoft.coffeeloyalty.ui.base.BaseFragment
import com.supersoft.coffeeloyalty.ui.utils.KeyboardUtils
import kotlinx.android.synthetic.main.fragment_password.vEtPassword
import kotlinx.android.synthetic.main.fragment_password.vIvClose

/**
 * Created by alex.lobur on 15.12.2019.
 * Email: lobur.a.y@gmail.com
 */
class PasswordFragment : BaseFragment<PasswordPresenter>(), PasswordView {

    override val layoutId: Int = R.layout.fragment_password

    @InjectPresenter
    lateinit var presenter: PasswordPresenter
    @ProvidePresenter
    fun providePresenter(): PasswordPresenter = presenterProvider.get()

    // -------------------------------
    //  Overridden (BaseFragment)
    // -------------------------------

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val targetScreen = arguments?.getSerializable(ARG_TARGET_SCREEN) as BaseScreen
        presenter.setTargetScreen(targetScreen)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewListeners()
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    // -------------------------------
    //  Overridden (PasswordView)
    // -------------------------------

    override fun clearPasswordText() {
        vEtPassword.text?.clear()
    }

    override fun focusOnPasswordTextInput() {
        vEtPassword.requestFocus()
        KeyboardUtils.showSoftKeyboard(requireContext(), vEtPassword)
    }

    // -------------------------------
    //  Private
    // -------------------------------

    private fun initViewListeners() {
        vIvClose.setOnClickListener {
            presenter.onCloseClick()
        }

        vEtPassword.addTextChangedListener(
            onTextChanged = { text, _, _, _ ->
                presenter.onPasswordTextChanged(text.toString())
            }
        )
    }

    companion object {
        private const val ARG_TARGET_SCREEN = "target_screen"

        @JvmStatic
        fun newInstance(targetScreen: BaseScreen): PasswordFragment {
            return PasswordFragment().apply {
                arguments = Bundle().also { args ->
                    args.putSerializable(ARG_TARGET_SCREEN, targetScreen)
                }
            }
        }
    }
}