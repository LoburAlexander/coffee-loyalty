package com.supersoft.coffeeloyalty.ui.settings

import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import androidx.appcompat.widget.Toolbar
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.supersoft.coffeeloyalty.R
import com.supersoft.coffeeloyalty.presentation.settings.SettingsPresenter
import com.supersoft.coffeeloyalty.presentation.settings.SettingsView
import com.supersoft.coffeeloyalty.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_activate_card.vToolbar
import kotlinx.android.synthetic.main.fragment_settings.vSbActionCountForReward
import kotlinx.android.synthetic.main.fragment_settings.vSbCardProcessDelay
import kotlinx.android.synthetic.main.fragment_settings.vTvActionCountForReward
import kotlinx.android.synthetic.main.fragment_settings.vTvCardProcessDelay

/**
 * Created by alex.lobur on 15.12.2019.
 * Email: lobur.a.y@gmail.com
 */
class SettingsFragment : BaseFragment<SettingsPresenter>(), SettingsView {

    override val layoutId: Int = R.layout.fragment_settings

    @InjectPresenter
    lateinit var presenter: SettingsPresenter
    @ProvidePresenter
    fun providePresenter(): SettingsPresenter = presenterProvider.get()

    // -------------------------------
    //  Overridden (BaseFragment)
    // -------------------------------

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initViewListeners()
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    // -------------------------------
    //  Overridden (SettingsView)
    // -------------------------------

    override fun setActionCountForRewardValue(value: Int) {
        val progress = mapActionCountForRewardSeekbarValueToProgress(value)
        vSbActionCountForReward.progress = progress
    }

    override fun setCardProcessDelayValue(value: Int) {
        val progress = mapCardProcessDelaySeekbarValueToProgress(value)
        vSbCardProcessDelay.progress = progress
    }

    override fun setActionCountForRewardText(text: String) {
        vTvActionCountForReward.text = text
    }

    override fun setCardProcessDelayText(text: String) {
        vTvCardProcessDelay.text = text
    }

    // -------------------------------
    //  Private
    // -------------------------------

    private fun initView() {
        initToolbar(vToolbar)
        vSbActionCountForReward.max = SEEKBAR_MAX_ACTION_COUNT_FOR_REWARD - SEEKBAR_MIN_ACTION_COUNT_FOR_REWARD
        vSbCardProcessDelay.max = SEEKBAR_MAX_CARD_PROCESS_DELAY - SEEKBAR_MIN_CARD_PROCESS_DELAY
    }

    private fun initViewListeners() {
        vSbActionCountForReward.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    val value = mapActionCountForRewardSeekbarProgressToValue(progress)
                    presenter.onActionCountForRewardValueChanged(value)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit

            override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit
        })

        vSbCardProcessDelay.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    val value = mapCardProcessDelaySeekbarProgressToValue(progress)
                    presenter.onCardProcessDelayValueChanged(value)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit

            override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit
        })
    }

    private fun initToolbar(toolbar: Toolbar) {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener {
            presenter.onBackPressed()
        }
    }

    private fun mapActionCountForRewardSeekbarProgressToValue(progress: Int): Int {
        return mapSeekbarProgressToValue(progress, SEEKBAR_MIN_ACTION_COUNT_FOR_REWARD)
    }

    private fun mapActionCountForRewardSeekbarValueToProgress(value: Int): Int {
        return mapSeekbarValueToProgress(value, SEEKBAR_MIN_ACTION_COUNT_FOR_REWARD)
    }

    private fun mapCardProcessDelaySeekbarProgressToValue(progress: Int): Int {
        return mapSeekbarProgressToValue(progress, SEEKBAR_MIN_CARD_PROCESS_DELAY)
    }

    private fun mapCardProcessDelaySeekbarValueToProgress(value: Int): Int {
        return mapSeekbarValueToProgress(value, SEEKBAR_MIN_CARD_PROCESS_DELAY)
    }

    private fun mapSeekbarProgressToValue(progress: Int, progressMin: Int): Int {
        return progress + progressMin
    }

    private fun mapSeekbarValueToProgress(value: Int, progressMin: Int): Int {
        return value - progressMin
    }

    companion object {
        private const val SEEKBAR_MIN_ACTION_COUNT_FOR_REWARD = 3
        private const val SEEKBAR_MAX_ACTION_COUNT_FOR_REWARD = 10
        private const val SEEKBAR_MIN_CARD_PROCESS_DELAY = 1
        private const val SEEKBAR_MAX_CARD_PROCESS_DELAY = 10

        @JvmStatic
        fun newInstance() = SettingsFragment()
    }
}