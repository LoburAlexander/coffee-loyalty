package com.supersoft.coffeeloyalty.ui.statistics

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.supersoft.coffeeloyalty.R
import com.supersoft.coffeeloyalty.presentation.statistics.StatisticsPresenter
import com.supersoft.coffeeloyalty.presentation.statistics.StatisticsView
import com.supersoft.coffeeloyalty.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_activate_card.vToolbar
import kotlinx.android.synthetic.main.fragment_statistics.vTvActionsCount
import kotlinx.android.synthetic.main.fragment_statistics.vTvActivatedCardsCount
import kotlinx.android.synthetic.main.fragment_statistics.vTvRewardReceivedCount

/**
 * Created by alex.lobur on 15.12.2019.
 * Email: lobur.a.y@gmail.com
 */
class StatisticsFragment : BaseFragment<StatisticsPresenter>(), StatisticsView {

    override val layoutId: Int = R.layout.fragment_statistics

    @InjectPresenter
    lateinit var presenter: StatisticsPresenter
    @ProvidePresenter
    fun providePresenter(): StatisticsPresenter = presenterProvider.get()

    // -------------------------------
    //  Overridden (BaseFragment)
    // -------------------------------

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    // -------------------------------
    //  Overridden (StatisticsPresenter)
    // -------------------------------

    override fun setActivatedCardsCountText(text: String) {
        vTvActivatedCardsCount.text = text
    }

    override fun setActionsCountText(text: String) {
        vTvActionsCount.text = text
    }

    override fun setRewardReceivedCountText(text: String) {
        vTvRewardReceivedCount.text = text
    }

    // -------------------------------
    //  Private
    // -------------------------------

    private fun initView() {
        initToolbar(vToolbar)
    }

    private fun initToolbar(toolbar: Toolbar) {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener {
            presenter.onBackPressed()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = StatisticsFragment()
    }
}