package com.supersoft.coffeeloyalty.ui.utils

import android.view.View
import com.google.android.material.snackbar.Snackbar
import com.supersoft.coffeeloyalty.ui.base.BaseFragment

/**
 * Created by alex.lobur on 14.12.2019.
 * Email: lobur.a.y@gmail.com
 */

fun BaseFragment<*>.showSnackbar(message: String, isShort: Boolean = true) {
    activity?.window?.decorView?.findViewById<View>(android.R.id.content)
        ?.let { activityDecorView ->
            val length = if (isShort) {
                Snackbar.LENGTH_SHORT
            } else {
                Snackbar.LENGTH_LONG
            }

            Snackbar.make(activityDecorView, message, length)
                .show()
        }
}