package com.supersoft.coffeeloyalty.ui.utils

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

/**
 * Created by alex.lobur on 22.12.2019.
 * Email: lobur.a.y@gmail.com
 */
class KeyboardUtils {

    companion object {

        fun showSoftKeyboard(context: Context, view: View) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }
}