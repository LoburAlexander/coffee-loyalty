package com.supersoft.coffeeloyalty.ui.utils

import android.view.View

/**
 * Created by alex.lobur on 14.12.2019.
 * Email: lobur.a.y@gmail.com
 */

fun View.makeVisible() {
    visibility = View.VISIBLE
}

fun View.makeInvisible() {
    visibility = View.INVISIBLE
}

fun View.makeGone() {
    visibility = View.GONE
}