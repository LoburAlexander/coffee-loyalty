package com.supersoft.coffeeloyalty.ui.utils.animation

import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.OvershootInterpolator
import android.view.animation.ScaleAnimation

/**
 * Created by alex.lobur on 15.12.2019.
 * Email: lobur.a.y@gmail.com
 */
class AnimationUtils {

    companion object {
        fun getRestAnimation(durationMillis: Long): Animation {
            return getAlphaAnimation(1f, 1f, durationMillis)
        }

        fun getFadeInAnimation(durationMillis: Long, speedFactor: Float = 1f): Animation {
            return getAlphaAnimation(0f, 1f, durationMillis, speedFactor)
        }

        fun getFadeOutAnimation(durationMillis: Long, speedFactor: Float = 1f): Animation {
            return getAlphaAnimation(1f, 0f, durationMillis, speedFactor)
        }

        fun getAlphaAnimation(
            startAlpha: Float,
            endAlpha: Float,
            durationMillis: Long,
            speedFactor: Float = 1f
        ): Animation {
            return AlphaAnimation(startAlpha, endAlpha).apply {
                duration = durationMillis
                interpolator = AccelerateInterpolator(speedFactor)
            }
        }

        fun getScaleInAnimation(durationMillis: Long): Animation {
            return getScaleAnimation(0.1f, 1f, durationMillis)
        }

        fun getScaleAnimation(startScale: Float, endScale: Float, durationMillis: Long): Animation {
            return ScaleAnimation(
                startScale, endScale, startScale, endScale,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f
            )
                .apply {
                    duration = durationMillis
                    interpolator = OvershootInterpolator()
                }
        }
    }
}