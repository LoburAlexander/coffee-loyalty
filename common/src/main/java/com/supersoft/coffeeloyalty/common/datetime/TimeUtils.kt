package com.supersoft.coffeeloyalty.common.datetime

/**
 * Created by alex.lobur on 15.09.2019.
 * Email: lobur.a.y@gmail.com
 */
class TimeUtils {

    companion object {

        fun currentMillis(): Long {
            return System.currentTimeMillis()
        }
    }
}