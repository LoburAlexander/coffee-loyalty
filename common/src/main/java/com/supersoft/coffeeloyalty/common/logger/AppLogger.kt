package com.supersoft.coffeeloyalty.common.logger

/**
 * Created by alex.lobur on 14.12.2019.
 * Email: lobur.a.y@gmail.com
 */

lateinit var logger: AppLogger

interface AppLogger {
    fun log(message: String)
    fun log(error: Throwable)
    fun log(message: String, error: Throwable)
}