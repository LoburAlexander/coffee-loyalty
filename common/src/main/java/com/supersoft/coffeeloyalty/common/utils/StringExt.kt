package com.supersoft.coffeeloyalty.common.utils

/**
 * Created by alex.lobur on 21.12.2019.
 * Email: lobur.a.y@gmail.com
 */

fun String.Companion.empty(): String = ""