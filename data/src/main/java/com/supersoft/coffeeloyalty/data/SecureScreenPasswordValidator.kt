package com.supersoft.coffeeloyalty.data

import com.supersoft.coffeeloyalty.domain.validator.ScreenPasswordValidator
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by alex.lobur on 22.12.2019.
 * Email: lobur.a.y@gmail.com
 */
class SecureScreenPasswordValidator @Inject constructor()
    : ScreenPasswordValidator {

    override fun isScreenPasswordCorrect(userText: String): Single<Boolean> {
        return Single.fromCallable {
            userText == BuildConfig.SCREEN_PASSWORD
        }
    }
}