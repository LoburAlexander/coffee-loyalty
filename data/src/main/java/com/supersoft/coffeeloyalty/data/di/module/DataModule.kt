package com.supersoft.coffeeloyalty.data.di.module

import dagger.Module

/**
 * Created by alex.lobur on 15.09.2019.
 * Email: lobur.a.y@gmail.com
 */
@Module(
    includes = [
        DbModule::class,
        RepositoryModule::class,
        ValidatorModule::class
    ]
)
abstract class DataModule