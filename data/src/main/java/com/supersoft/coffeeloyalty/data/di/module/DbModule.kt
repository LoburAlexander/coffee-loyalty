package com.supersoft.coffeeloyalty.data.di.module

import android.content.Context
import androidx.room.Room
import com.supersoft.coffeeloyalty.data.room.dao.LoyaltyCardDao
import com.supersoft.coffeeloyalty.data.room.dao.LoyaltyCardEventDao
import com.supersoft.coffeeloyalty.data.room.database.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by alex.lobur on 15.09.2019.
 * Email: lobur.a.y@gmail.com
 */
@Module
class DbModule {

    @Provides
    @Singleton
    fun provideLoyaltyCardDao(database: AppDatabase): LoyaltyCardDao {
        return database.loyaltyCardDao()
    }

    @Provides
    @Singleton
    fun provideLoyaltyCardEventDao(database: AppDatabase): LoyaltyCardEventDao {
        return database.loyaltyCardEventDao()
    }

    @Provides
    @Singleton
    fun provideDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(
                context,
                AppDatabase::class.java,
                "app-db"
        ).build()
    }
}