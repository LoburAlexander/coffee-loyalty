package com.supersoft.coffeeloyalty.data.di.module

import com.supersoft.coffeeloyalty.data.repository.RoomLoyaltyCardEventRepository
import com.supersoft.coffeeloyalty.data.repository.RoomLoyaltyCardRepository
import com.supersoft.coffeeloyalty.data.repository.preferences.PreferencesAppSettingsRepository
import com.supersoft.coffeeloyalty.domain.repository.AppSettingsRepository
import com.supersoft.coffeeloyalty.domain.repository.LoyaltyCardEventRepository
import com.supersoft.coffeeloyalty.domain.repository.LoyaltyCardRepository
import dagger.Binds
import dagger.Module

/**
 * Created by alex.lobur on 16.09.2019.
 * Email: lobur.a.y@gmail.com
 */
@Module
interface RepositoryModule {

    @Binds
    fun bindAppSettingsRepository(repository: PreferencesAppSettingsRepository): AppSettingsRepository

    @Binds
    fun bindLoyaltyCardRepository(repository: RoomLoyaltyCardRepository): LoyaltyCardRepository

    @Binds
    fun bindLoyaltyCardEventRepository(repository: RoomLoyaltyCardEventRepository): LoyaltyCardEventRepository
}