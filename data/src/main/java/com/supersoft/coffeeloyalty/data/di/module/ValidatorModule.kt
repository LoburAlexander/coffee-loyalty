package com.supersoft.coffeeloyalty.data.di.module

import com.supersoft.coffeeloyalty.data.SecureScreenPasswordValidator
import com.supersoft.coffeeloyalty.domain.validator.ScreenPasswordValidator
import dagger.Binds
import dagger.Module

/**
 * Created by alex.lobur on 16.09.2019.
 * Email: lobur.a.y@gmail.com
 */
@Module
interface ValidatorModule {

    @Binds
    fun bindScreenPasswordValidator(validator: SecureScreenPasswordValidator): ScreenPasswordValidator
}