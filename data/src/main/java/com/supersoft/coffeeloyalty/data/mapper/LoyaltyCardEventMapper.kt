package com.supersoft.coffeeloyalty.data.mapper

import com.supersoft.coffeeloyalty.data.room.entity.LoyaltyCardEventRoom
import com.supersoft.coffeeloyalty.data.room.entity.LoyaltyCardRoom
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCard
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCardEvent
import javax.inject.Inject

/**
 * Created by alex.lobur on 16.09.2019.
 * Email: lobur.a.y@gmail.com
 */

class LoyaltyCardEventMapper
@Inject
constructor(
    private val loyaltyCardEventTypeMapper: LoyaltyCardEventTypeMapper
) {

    fun mapRoomToDomain(roomEvent: LoyaltyCardEventRoom): LoyaltyCardEvent {
        val domainEventType = loyaltyCardEventTypeMapper.mapRoomToDomain(roomEvent.eventType)
        return LoyaltyCardEvent(
            id = roomEvent.id,
            cardId = roomEvent.cardId,
            eventType = domainEventType,
            createdAt = roomEvent.createdAt
        )
    }

    fun mapDomainToRoom(domainEvent: LoyaltyCardEvent): LoyaltyCardEventRoom {
        val roomEventType = loyaltyCardEventTypeMapper.mapDomainToRoom(domainEvent.eventType)
        return LoyaltyCardEventRoom(
            id = domainEvent.id,
            cardId = domainEvent.cardId,
            eventType = roomEventType,
            createdAt = domainEvent.createdAt
        )
    }

}