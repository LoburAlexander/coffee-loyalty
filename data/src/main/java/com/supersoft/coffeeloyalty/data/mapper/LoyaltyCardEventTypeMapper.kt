package com.supersoft.coffeeloyalty.data.mapper

import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCardEventType
import javax.inject.Inject

/**
 * Created by alex.lobur on 16.09.2019.
 * Email: lobur.a.y@gmail.com
 */

class LoyaltyCardEventTypeMapper
@Inject
constructor() {

    fun mapRoomToDomain(roomEventType: String): LoyaltyCardEventType {
        return LoyaltyCardEventType.valueOf(roomEventType)
    }

    fun mapDomainToRoom(domainEventType: LoyaltyCardEventType): String {
        return domainEventType.toString()
    }

}