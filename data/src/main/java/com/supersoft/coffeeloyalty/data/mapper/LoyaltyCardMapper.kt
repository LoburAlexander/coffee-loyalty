package com.supersoft.coffeeloyalty.data.mapper

import com.supersoft.coffeeloyalty.data.room.entity.LoyaltyCardRoom
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCard
import javax.inject.Inject

/**
 * Created by alex.lobur on 16.09.2019.
 * Email: lobur.a.y@gmail.com
 */

class LoyaltyCardMapper
@Inject
constructor() {

    fun mapRoomToDomain(roomCard: LoyaltyCardRoom): LoyaltyCard {
        return LoyaltyCard(
            id = roomCard.id,
            tagId = roomCard.tagId,
            rewardedActionCount = roomCard.rewardedActionCount,
            currentActionCount = roomCard.currentActionCount,
            createdAt = roomCard.createdAt
        )
    }

    fun mapDomainToRoom(domainCard: LoyaltyCard): LoyaltyCardRoom {
        return LoyaltyCardRoom(
            id = domainCard.id,
            tagId = domainCard.tagId,
            rewardedActionCount = domainCard.rewardedActionCount,
            currentActionCount = domainCard.currentActionCount,
            createdAt = domainCard.createdAt
        )
    }

}