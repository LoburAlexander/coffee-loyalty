package com.supersoft.coffeeloyalty.data.repository

import com.supersoft.coffeeloyalty.data.mapper.LoyaltyCardEventMapper
import com.supersoft.coffeeloyalty.data.mapper.LoyaltyCardEventTypeMapper
import com.supersoft.coffeeloyalty.data.room.dao.LoyaltyCardEventDao
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCardEvent
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCardEventType
import com.supersoft.coffeeloyalty.domain.repository.LoyaltyCardEventRepository
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by alex.lobur on 16.09.2019.
 * Email: lobur.a.y@gmail.com
 */
@Singleton
class RoomLoyaltyCardEventRepository
@Inject
constructor(
    private val loyaltyCardEventDao: LoyaltyCardEventDao,
    private val loyaltyCardEventTypeMapper: LoyaltyCardEventTypeMapper,
    private val loyaltyCardEventMapper: LoyaltyCardEventMapper
) : LoyaltyCardEventRepository {

    override fun getEventsCount(eventType: LoyaltyCardEventType): Single<Int> {
        return Single.just(eventType)
            .map(loyaltyCardEventTypeMapper::mapDomainToRoom)
            .flatMap(loyaltyCardEventDao::selectEventsCount)
    }

    override fun insertEvent(cardEvent: LoyaltyCardEvent): Single<LoyaltyCardEvent> {
        return Single.just(cardEvent)
            .map(loyaltyCardEventMapper::mapDomainToRoom)
            .flatMap(loyaltyCardEventDao::insert)
            .map { roomEventId ->
                cardEvent.copyWithId(roomEventId)
            }
    }

    override fun deleteEventsByCardId(cardId: String): Completable {
        return loyaltyCardEventDao.deleteByCardId(cardId)
    }
}