package com.supersoft.coffeeloyalty.data.repository

import com.supersoft.coffeeloyalty.data.mapper.LoyaltyCardMapper
import com.supersoft.coffeeloyalty.data.room.dao.LoyaltyCardDao
import com.supersoft.coffeeloyalty.data.room.entity.LoyaltyCardRoom
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCard
import com.supersoft.coffeeloyalty.domain.error.card.db.CardNotFoundError
import com.supersoft.coffeeloyalty.domain.repository.LoyaltyCardRepository
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by alex.lobur on 16.09.2019.
 * Email: lobur.a.y@gmail.com
 */
@Singleton
class RoomLoyaltyCardRepository
@Inject
constructor(
    private val loyaltyCardDao: LoyaltyCardDao,
    private val loyaltyCardMapper: LoyaltyCardMapper
) : LoyaltyCardRepository {

    override fun getCardById(id: String): Maybe<LoyaltyCard> {
        return loyaltyCardDao.readCardById(id)
            .map(loyaltyCardMapper::mapRoomToDomain)
    }

    override fun insertCard(card: LoyaltyCard): Single<LoyaltyCard> {
        return Single.just(card)
            .map(loyaltyCardMapper::mapDomainToRoom)
            .flatMap(loyaltyCardDao::insert)
            .map { card }
    }

    override fun updateCardCurrentActions(
        cardId: String,
        currentActions: Int
    ): Single<LoyaltyCard> {
        return loyaltyCardDao.readCardById(cardId)
            .switchIfEmpty(Single.error<LoyaltyCardRoom>(CardNotFoundError()))
            .flatMap { cardRoom ->
                val updatedCardRoom = LoyaltyCardRoom(
                    id = cardRoom.id,
                    tagId = cardRoom.tagId,
                    rewardedActionCount = cardRoom.rewardedActionCount,
                    currentActionCount = currentActions,
                    createdAt = cardRoom.createdAt
                )

                loyaltyCardDao.insertOrUpdate(updatedCardRoom)
                    .map { updatedCardRoom }
            }
            .map(loyaltyCardMapper::mapRoomToDomain)
    }

    override fun deleteCardCompletely(cardId: String): Completable {
        return loyaltyCardDao.deleteById(cardId)
    }
}