package com.supersoft.coffeeloyalty.data.repository.preferences

import android.content.Context
import android.content.SharedPreferences
import com.supersoft.coffeeloyalty.data.utils.delegates.PreferencesDelegate
import com.supersoft.coffeeloyalty.domain.repository.AppSettingsRepository
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by alex.lobur on 21.12.2019.
 * Email: lobur.a.y@gmail.com
 */
@Singleton
class PreferencesAppSettingsRepository @Inject constructor(
    appContext: Context
) : AppSettingsRepository {

    private val preferences: SharedPreferences =
        appContext.getSharedPreferences(APP_PREFERENCES_NAME, Context.MODE_PRIVATE)

    override var actionCountForReward: Int
            by InnerPreferencesDelegate(
                KEY_ACTION_COUNT_FOR_REWARD,
                DEFAULT_ACTION_COUNT_FOR_REWARD
            )

    override var cardProcessDelaySeconds: Int
            by InnerPreferencesDelegate(
                KEY_CARD_PROCESS_DELAY_SECONDS,
                DEFAULT_CARD_PROCESS_DELAY_SECONDS
            )


    private inner class InnerPreferencesDelegate<TValue : Any>(
        key: String,
        defaultValue: TValue
    ) : PreferencesDelegate<TValue>(preferences, key, defaultValue)


    companion object {
        private const val APP_PREFERENCES_NAME: String = "app_preferences"

        private const val DEFAULT_ACTION_COUNT_FOR_REWARD: Int = 5
        private const val DEFAULT_CARD_PROCESS_DELAY_SECONDS: Int = 3

        private const val KEY_ACTION_COUNT_FOR_REWARD: String = "action_count_for_reward"
        private const val KEY_CARD_PROCESS_DELAY_SECONDS: String = "card_process_delay_seconds"
    }
}