package com.supersoft.coffeeloyalty.data.room.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.supersoft.coffeeloyalty.data.room.entity.LoyaltyCardRoom
import com.supersoft.coffeeloyalty.data.room.schema.RoomColumns
import com.supersoft.coffeeloyalty.data.room.schema.RoomTables
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

/**
 * Created by alex.lobur on 15.09.2019.
 * Email: lobur.a.y@gmail.com
 */
@Dao
interface LoyaltyCardDao {

    @Query("SELECT * FROM ${RoomTables.LOYALTY_CARD}")
    fun readAllCards(): Single<List<LoyaltyCardRoom>>

    @Query("SELECT * FROM ${RoomTables.LOYALTY_CARD} WHERE ${RoomColumns.LoyaltyCard.ID} = :cardId")
    fun readCardById(cardId: String): Maybe<LoyaltyCardRoom>

    @Insert
    fun insert(card: LoyaltyCardRoom): Single<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdate(card: LoyaltyCardRoom): Single<Long>

    @Delete
    fun delete(card: LoyaltyCardRoom): Completable

    @Query("DELETE FROM ${RoomTables.LOYALTY_CARD} WHERE ${RoomColumns.LoyaltyCard.ID} = :cardId")
    fun deleteById(cardId: String): Completable
}