package com.supersoft.coffeeloyalty.data.room.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.supersoft.coffeeloyalty.data.room.entity.LoyaltyCardEventRoom
import com.supersoft.coffeeloyalty.data.room.schema.RoomColumns
import com.supersoft.coffeeloyalty.data.room.schema.RoomTables
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCardEventType
import io.reactivex.Completable
import io.reactivex.Single

/**
 * Created by alex.lobur on 15.09.2019.
 * Email: lobur.a.y@gmail.com
 */
@Dao
interface LoyaltyCardEventDao {

    @Query("SELECT COUNT(*) FROM ${RoomTables.LOYALTY_CARD_EVENT} WHERE ${RoomColumns.LoyaltyCardEvent.EVENT_TYPE} = :eventType")
    fun selectEventsCount(eventType: String): Single<Int>

    @Insert
    fun insert(event: LoyaltyCardEventRoom): Single<Long>

    @Delete
    fun delete(card: LoyaltyCardEventRoom): Completable

    @Query("DELETE FROM ${RoomTables.LOYALTY_CARD_EVENT} WHERE ${RoomColumns.LoyaltyCardEvent.CARD_ID} = :cardId")
    fun deleteByCardId(cardId: String): Completable
}