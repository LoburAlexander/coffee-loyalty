package com.supersoft.coffeeloyalty.data.room.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.supersoft.coffeeloyalty.data.room.dao.LoyaltyCardDao
import com.supersoft.coffeeloyalty.data.room.dao.LoyaltyCardEventDao
import com.supersoft.coffeeloyalty.data.room.entity.LoyaltyCardEventRoom
import com.supersoft.coffeeloyalty.data.room.entity.LoyaltyCardRoom

/**
 * Created by alex.lobur on 15.09.2019.
 * Email: lobur.a.y@gmail.com
 */
@Database(entities = arrayOf(
        LoyaltyCardRoom::class,
        LoyaltyCardEventRoom::class
), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun loyaltyCardDao(): LoyaltyCardDao
    abstract fun loyaltyCardEventDao(): LoyaltyCardEventDao
}