package com.supersoft.coffeeloyalty.data.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.supersoft.coffeeloyalty.data.room.schema.RoomColumns
import com.supersoft.coffeeloyalty.data.room.schema.RoomTables

/**
 * Created by alex.lobur on 15.09.2019.
 * Email: lobur.a.y@gmail.com
 */
@Entity(tableName = RoomTables.LOYALTY_CARD_EVENT)
data class LoyaltyCardEventRoom(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = RoomColumns.LoyaltyCardEvent.ID)
    val id: Long,

    @ColumnInfo(name = RoomColumns.LoyaltyCardEvent.CARD_ID)
    val cardId: String,

    @ColumnInfo(name = RoomColumns.LoyaltyCardEvent.EVENT_TYPE)
    val eventType: String,

    @ColumnInfo(name = RoomColumns.LoyaltyCardEvent.CREATED_AT)
    val createdAt: Long
)