package com.supersoft.coffeeloyalty.data.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.supersoft.coffeeloyalty.data.room.schema.RoomColumns
import com.supersoft.coffeeloyalty.data.room.schema.RoomTables

/**
 * Created by alex.lobur on 15.09.2019.
 * Email: lobur.a.y@gmail.com
 */
@Entity(tableName = RoomTables.LOYALTY_CARD)
data class LoyaltyCardRoom(
    @PrimaryKey
    val id: String,

    @ColumnInfo(name = RoomColumns.LoyaltyCard.TAG_ID)
    val tagId: String?,

    @ColumnInfo(name = RoomColumns.LoyaltyCard.REWARDED_ACTION_COUNT)
    val rewardedActionCount: Int,

    @ColumnInfo(name = RoomColumns.LoyaltyCard.CURRENT_ACTION_COUNT)
    val currentActionCount: Int,

    @ColumnInfo(name = RoomColumns.LoyaltyCard.CREATED_AT)
    val createdAt: Long
)