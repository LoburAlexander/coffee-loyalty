package com.supersoft.coffeeloyalty.data.room.schema

class RoomTables {
    companion object {
        const val LOYALTY_CARD: String = "loyalty_card"
        const val LOYALTY_CARD_EVENT: String = "loyalty_card_event"
    }
}

sealed class RoomColumns {
    class LoyaltyCard {
        companion object {
            const val ID: String = "id"
            const val TAG_ID: String = "tag_id"
            const val REWARDED_ACTION_COUNT: String = "rewarded_action_count"
            const val CURRENT_ACTION_COUNT: String = "current_action_count"
            const val CREATED_AT: String = "created_at"
        }
    }

    class LoyaltyCardEvent {
        companion object {
            const val ID: String = "id"
            const val CARD_ID: String = "card_id"
            const val EVENT_TYPE: String = "event_type"
            const val CREATED_AT: String = "created_at"
        }
    }
}