package com.supersoft.coffeeloyalty.data.utils

/**
 * Created by alex.lobur on 08.09.2019.
 * Email: lobur.a.y@gmail.com
 */
class NfcTagUtils {
    companion object {
        @JvmStatic
        fun tagIdBytesToString(bytes: ByteArray): String {
            return bytes.joinToString(separator = ":") { byte ->
                "%02x".format(byte)
            }
        }

        @JvmStatic
        fun tagIdStringToBytes(string: String): ByteArray {
            return string.split(":")
                .map { it.toInt(16).toByte() }
                .toByteArray()
        }
    }
}