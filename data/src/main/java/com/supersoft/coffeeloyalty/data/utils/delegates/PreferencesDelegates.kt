package com.supersoft.coffeeloyalty.data.utils.delegates

import android.content.SharedPreferences
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * Created by alex.lobur on 21.12.2019.
 * Email: lobur.a.y@gmail.com
 */
open class PreferencesDelegate<TValue : Any>(
    private val preferences: SharedPreferences,
    private val key: String,
    private val defaultValue: TValue
) : ReadWriteProperty<Any?, TValue> {

    @Suppress("UNCHECKED_CAST")
    override fun getValue(thisRef: Any?, property: KProperty<*>): TValue {
        with(preferences) {
            return when (defaultValue) {
                is Boolean -> getBoolean(key, defaultValue) as TValue
                is Int -> getInt(key, defaultValue) as TValue
                is Float -> getFloat(key, defaultValue) as TValue
                is Long -> getLong(key, defaultValue) as TValue
                is String -> getString(key, defaultValue) as TValue
                else -> throw IllegalStateException("Unsupported preference type: ${defaultValue::class}")
            }
        }
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: TValue) {
        with(preferences.edit()) {
            when (value) {
                is Boolean -> putBoolean(key, value)
                is Int -> putInt(key, value)
                is Float -> putFloat(key, value)
                is Long -> putLong(key, value)
                is String -> putString(key, value)
                else -> throw IllegalStateException("Unsupported preference type: ${defaultValue::class}")
            }
            apply()
        }
    }
}