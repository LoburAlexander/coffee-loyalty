package com.supersoft.coffeeloyalty.domain.entity

/**
 * Created by alex.lobur on 15.09.2019.
 * Email: lobur.a.y@gmail.com
 */
data class LoyaltyCard(
    val id: String,
    val tagId: String?,
    val rewardedActionCount: Int,
    val currentActionCount: Int,
    val createdAt: Long
) {
    val remainingActions = rewardedActionCount - currentActionCount
}