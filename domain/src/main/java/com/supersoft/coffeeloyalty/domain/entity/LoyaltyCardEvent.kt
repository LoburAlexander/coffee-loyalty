package com.supersoft.coffeeloyalty.domain.entity

/**
 * Created by alex.lobur on 15.09.2019.
 * Email: lobur.a.y@gmail.com
 */
data class LoyaltyCardEvent(
    val id: Long,
    val cardId: String,
    val eventType: LoyaltyCardEventType,
    val createdAt: Long
) {

    fun copyWithId(id: Long): LoyaltyCardEvent {
        return LoyaltyCardEvent(
            id = id,
            cardId = cardId,
            eventType = eventType,
            createdAt = createdAt
        )
    }

    companion object {
        const val ID_AUTO_GENERATE: Long = 0L
    }
}