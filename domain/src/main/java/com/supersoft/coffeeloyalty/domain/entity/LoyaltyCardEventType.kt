package com.supersoft.coffeeloyalty.domain.entity

/**
 * Created by alex.lobur on 15.09.2019.
 * Email: lobur.a.y@gmail.com
 */
enum class LoyaltyCardEventType {
    ACTIVATED,
    ACTION_PERFORMED,
    REWARD_RECEIVED,
    RESET
}