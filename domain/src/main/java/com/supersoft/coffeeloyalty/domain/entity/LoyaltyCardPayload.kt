package com.supersoft.coffeeloyalty.domain.entity

/**
 * Created by alex.lobur on 15.09.2019.
 * Email: lobur.a.y@gmail.com
 */
data class LoyaltyCardPayload(
    val tagId: String?,
    val cardId: String?
)