package com.supersoft.coffeeloyalty.domain.error.card

import java.lang.RuntimeException

/**
 * Created by alex.lobur on 15.09.2019.
 * Email: lobur.a.y@gmail.com
 */
class CardAlreadyActivatedError : RuntimeException()