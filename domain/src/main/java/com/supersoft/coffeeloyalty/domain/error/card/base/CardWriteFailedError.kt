package com.supersoft.coffeeloyalty.domain.error.card.base

import java.lang.RuntimeException

/**
 * Created by alex.lobur on 05.10.2019.
 * Email: lobur.a.y@gmail.com
 */
class CardWriteFailedError : RuntimeException {
    constructor() : super()
    constructor(p0: String?) : super(p0)
    constructor(p0: String?, p1: Throwable?) : super(p0, p1)
}