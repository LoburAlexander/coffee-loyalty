package com.supersoft.coffeeloyalty.domain.relays.cardconnect

/**
 * Created by alex.lobur on 05.10.2019.
 * Email: lobur.a.y@gmail.com
 */
interface CardConnectedListener {
    fun onCardConnected(cardConnection: CardConnection)
}