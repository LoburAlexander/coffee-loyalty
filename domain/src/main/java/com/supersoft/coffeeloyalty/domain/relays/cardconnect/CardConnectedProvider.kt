package com.supersoft.coffeeloyalty.domain.relays.cardconnect

import io.reactivex.Observable

/**
 * Created by alex.lobur on 05.10.2019.
 * Email: lobur.a.y@gmail.com
 */
interface CardConnectedProvider {
    val cardConnectedObs: Observable<CardConnection>
}