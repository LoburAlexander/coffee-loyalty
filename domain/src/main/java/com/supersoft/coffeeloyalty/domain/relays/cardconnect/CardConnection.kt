package com.supersoft.coffeeloyalty.domain.relays.cardconnect

import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCardPayload
import io.reactivex.Completable
import io.reactivex.Single

/**
 * Created by alex.lobur on 05.10.2019.
 * Email: lobur.a.y@gmail.com
 */
abstract class CardConnection {
    abstract fun readCardPayload(): Single<LoyaltyCardPayload>
    abstract fun writeCardPayload(payload: LoyaltyCardPayload): Completable
}