package com.supersoft.coffeeloyalty.domain.repository

/**
 * Created by alex.lobur on 21.12.2019.
 * Email: lobur.a.y@gmail.com
 */
interface AppSettingsRepository {

    /**
     * Number of actions to be performed by the client to receive a reward.
     */
    var actionCountForReward: Int

    /**
     * Delay between two subsequent loyalty card processing (in seconds).
     * It is needed to prevent fraud, when a user taps a card multiple times in a short period of time.
     */
    var cardProcessDelaySeconds: Int
}