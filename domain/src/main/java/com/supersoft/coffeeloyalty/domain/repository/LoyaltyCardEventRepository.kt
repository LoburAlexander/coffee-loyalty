package com.supersoft.coffeeloyalty.domain.repository

import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCardEvent
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCardEventType
import io.reactivex.Completable
import io.reactivex.Single

/**
 * Created by alex.lobur on 15.09.2019.
 * Email: lobur.a.y@gmail.com
 */
interface LoyaltyCardEventRepository {

    fun getEventsCount(eventType: LoyaltyCardEventType): Single<Int>

    fun insertEvent(cardEvent: LoyaltyCardEvent): Single<LoyaltyCardEvent>

    fun deleteEventsByCardId(cardId: String): Completable
}