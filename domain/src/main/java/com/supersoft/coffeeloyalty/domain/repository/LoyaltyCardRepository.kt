package com.supersoft.coffeeloyalty.domain.repository

import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCard
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

/**
 * Created by alex.lobur on 15.09.2019.
 * Email: lobur.a.y@gmail.com
 */
interface LoyaltyCardRepository {

    fun getCardById(id: String): Maybe<LoyaltyCard>

    fun insertCard(card: LoyaltyCard): Single<LoyaltyCard>

    fun updateCardCurrentActions(cardId: String, currentActions: Int): Single<LoyaltyCard>

    fun deleteCardCompletely(cardId: String): Completable
}