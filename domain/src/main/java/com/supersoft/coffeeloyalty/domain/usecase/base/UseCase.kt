package com.supersoft.coffeeloyalty.domain.usecase.base

/**
 * Created by alex.lobur on 15.09.2019.
 * Email: lobur.a.y@gmail.com
 */
interface UseCase<InputT, OutputT> {
    fun execute(input: InputT): OutputT
}