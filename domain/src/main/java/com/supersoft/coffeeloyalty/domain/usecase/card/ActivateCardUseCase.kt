package com.supersoft.coffeeloyalty.domain.usecase.card

import com.supersoft.coffeeloyalty.common.datetime.TimeUtils
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCard
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCardEventType
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCardPayload
import com.supersoft.coffeeloyalty.domain.error.card.CardAlreadyActivatedError
import com.supersoft.coffeeloyalty.domain.repository.AppSettingsRepository
import com.supersoft.coffeeloyalty.domain.repository.LoyaltyCardRepository
import com.supersoft.coffeeloyalty.domain.usecase.base.UseCase
import com.supersoft.coffeeloyalty.domain.usecase.card.event.AddLoyaltyCardEventUseCase
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.functions.Function
import java.util.UUID
import javax.inject.Inject

/**
 * Created by alex.lobur on 15.09.2019.
 * Email: lobur.a.y@gmail.com
 */
class ActivateCardUseCase
@Inject
constructor(
    private val addLoyaltyCardEvent: AddLoyaltyCardEventUseCase,
    private val loyaltyCardRepository: LoyaltyCardRepository,
    appSettingsRepository: AppSettingsRepository
) : UseCase<LoyaltyCardPayload, Single<LoyaltyCard>> {

    private val actionCountForReward: Int = appSettingsRepository.actionCountForReward

    override fun execute(input: LoyaltyCardPayload): Single<LoyaltyCard> {
        val existingCard = if (input.cardId != null) {
            loyaltyCardRepository.getCardById(input.cardId)
        } else {
            Maybe.empty()
        }

        return existingCard
            .map(Function<LoyaltyCard, LoyaltyCard> {
                throw CardAlreadyActivatedError()
            })
            .switchIfEmpty(
                Single.defer {
                    val cardInfo = LoyaltyCard(
                        id = generateCardId(),
                        tagId = input.tagId,
                        rewardedActionCount = actionCountForReward,
                        currentActionCount = 0,
                        createdAt = TimeUtils.currentMillis()
                    )

                    return@defer loyaltyCardRepository.insertCard(cardInfo)
                }
            )
            .flatMap { card ->
                val addEventInput = AddLoyaltyCardEventUseCase.Input(
                    cardId = card.id,
                    eventType = LoyaltyCardEventType.ACTIVATED
                )
                addLoyaltyCardEvent.execute(addEventInput)
                    .andThen(Single.just(card))
            }
    }

    private fun generateCardId(): String {
        return UUID.randomUUID().toString()
    }
}