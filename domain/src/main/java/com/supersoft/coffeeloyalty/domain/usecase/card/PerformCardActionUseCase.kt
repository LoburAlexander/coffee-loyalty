package com.supersoft.coffeeloyalty.domain.usecase.card

import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCard
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCardEventType
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCardPayload
import com.supersoft.coffeeloyalty.domain.error.card.CardNotActivatedError
import com.supersoft.coffeeloyalty.domain.repository.LoyaltyCardRepository
import com.supersoft.coffeeloyalty.domain.usecase.base.UseCase
import com.supersoft.coffeeloyalty.domain.usecase.card.event.AddLoyaltyCardEventUseCase
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by alex.lobur on 12.10.2019.
 * Email: lobur.a.y@gmail.com
 */
class PerformCardActionUseCase
@Inject
constructor(
    private val addLoyaltyCardEvent: AddLoyaltyCardEventUseCase,
    private val resetCardActionsUseCase: ResetCardActionsUseCase,
    private val loyaltyCardRepository: LoyaltyCardRepository
) : UseCase<LoyaltyCardPayload, Single<Int>> {

    /**
     * @return Remaining actions count to get reward.
     */
    override fun execute(input: LoyaltyCardPayload): Single<Int> {
        if (input.cardId == null) {
            return Single.error<Int>(CardNotActivatedError())
        }

        val cardSingle = loyaltyCardRepository.getCardById(input.cardId)
            .switchIfEmpty(
                Single.error<LoyaltyCard>(CardNotActivatedError())
            )

        return cardSingle
            .flatMap { card ->
                addCardEvent(card, LoyaltyCardEventType.ACTION_PERFORMED)
                    .andThen(Single.just(card))
            }
            .flatMap { card ->
                if (card.remainingActions < 1) {
                    // Shouldn't happen normally
                    resetCardActionsUseCase.execute(card.id)
                        .map(LoyaltyCard::remainingActions)
                } else {
                    val currentActions = card.currentActionCount + 1
                    if (currentActions >= card.rewardedActionCount) {
                        // Reward achieved, reset card and return 0 remaining actions
                        addCardEvent(card, LoyaltyCardEventType.REWARD_RECEIVED)
                            .andThen(Single.defer {
                                resetCardActionsUseCase.execute(card.id)
                                    .map { 0 }
                            })
                    } else {
                        // Reward not achieved, track new card action and return remaining actions
                        loyaltyCardRepository.updateCardCurrentActions(card.id, currentActions)
                            .map(LoyaltyCard::remainingActions)
                    }
                }
            }
    }

    private fun addCardEvent(card: LoyaltyCard, eventType: LoyaltyCardEventType): Completable {
        val addEventInput = AddLoyaltyCardEventUseCase.Input(
            cardId = card.id,
            eventType = eventType
        )
        return addLoyaltyCardEvent.execute(addEventInput)
    }
}