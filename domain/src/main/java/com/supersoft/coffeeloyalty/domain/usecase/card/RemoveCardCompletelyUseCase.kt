package com.supersoft.coffeeloyalty.domain.usecase.card

import com.supersoft.coffeeloyalty.common.logger.logger
import com.supersoft.coffeeloyalty.domain.repository.LoyaltyCardEventRepository
import com.supersoft.coffeeloyalty.domain.repository.LoyaltyCardRepository
import com.supersoft.coffeeloyalty.domain.usecase.base.UseCase
import io.reactivex.Completable
import javax.inject.Inject

/**
 * Created by alex.lobur on 05.10.2019.
 * Email: lobur.a.y@gmail.com
 */
class RemoveCardCompletelyUseCase
@Inject
constructor(
    private val loyaltyCardRepository: LoyaltyCardRepository,
    private val loyaltyCardEventRepository: LoyaltyCardEventRepository
) : UseCase<String, Completable> {

    @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
    override fun execute(cardId: String): Completable {
        return loyaltyCardRepository.deleteCardCompletely(cardId)
            .doOnComplete {
                logger.log("Card deleted. Card id: $cardId")
            }
            .andThen(
                Completable.defer {
                    loyaltyCardEventRepository.deleteEventsByCardId(cardId)
                }
                    .doOnComplete {
                        logger.log("Card events deleted. Card id: $cardId")
                    }
            )
    }
}