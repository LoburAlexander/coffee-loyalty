package com.supersoft.coffeeloyalty.domain.usecase.card

import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCard
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCardEventType
import com.supersoft.coffeeloyalty.domain.repository.LoyaltyCardRepository
import com.supersoft.coffeeloyalty.domain.usecase.base.UseCase
import com.supersoft.coffeeloyalty.domain.usecase.card.event.AddLoyaltyCardEventUseCase
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by alex.lobur on 12.10.2019.
 * Email: lobur.a.y@gmail.com
 */
class ResetCardActionsUseCase
@Inject
constructor(
    private val addLoyaltyCardEvent: AddLoyaltyCardEventUseCase,
    private val loyaltyCardRepository: LoyaltyCardRepository
) : UseCase<String, Single<LoyaltyCard>> {

    @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
    override fun execute(cardId: String): Single<LoyaltyCard> {
        return loyaltyCardRepository.updateCardCurrentActions(cardId, 0)
            .flatMap { card ->
                val addEventInput = AddLoyaltyCardEventUseCase.Input(
                    cardId = card.id,
                    eventType = LoyaltyCardEventType.RESET
                )
                addLoyaltyCardEvent.execute(addEventInput)
                    .andThen(Single.just(card))
            }
    }
}