package com.supersoft.coffeeloyalty.domain.usecase.card.event

import com.supersoft.coffeeloyalty.common.datetime.TimeUtils
import com.supersoft.coffeeloyalty.common.logger.logger
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCardEvent
import com.supersoft.coffeeloyalty.domain.entity.LoyaltyCardEventType
import com.supersoft.coffeeloyalty.domain.repository.LoyaltyCardEventRepository
import com.supersoft.coffeeloyalty.domain.usecase.base.UseCase
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by alex.lobur on 22.12.2019.
 * Email: lobur.a.y@gmail.com
 */
class AddLoyaltyCardEventUseCase
@Inject
constructor(
    private val loyaltyCardEventRepository: LoyaltyCardEventRepository
) : UseCase<AddLoyaltyCardEventUseCase.Input, Completable> {

    override fun execute(input: Input): Completable {
        return Single.fromCallable {
            val timestamp = TimeUtils.currentMillis()

            LoyaltyCardEvent(
                id = LoyaltyCardEvent.ID_AUTO_GENERATE,
                cardId = input.cardId,
                eventType = input.eventType,
                createdAt = timestamp
            )
        }
            .flatMapCompletable { cardEvent ->
                loyaltyCardEventRepository.insertEvent(cardEvent)
                    .doOnSuccess { addedCardEvent ->
                        logger.log("Card event added. $addedCardEvent")
                    }
                    .ignoreElement()
            }
    }

    data class Input(
        val cardId: String,
        val eventType: LoyaltyCardEventType
    )
}