package com.supersoft.coffeeloyalty.domain.usecase.password

import com.supersoft.coffeeloyalty.domain.usecase.base.UseCase
import com.supersoft.coffeeloyalty.domain.validator.ScreenPasswordValidator
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by alex.lobur on 15.09.2019.
 * Email: lobur.a.y@gmail.com
 */
class ValidateScreenPasswordUseCase
@Inject
constructor(
    private val screenPasswordValidator: ScreenPasswordValidator
) : UseCase<String, Single<Boolean>> {

    /**
     * @return Is screen password correct.
     */
    @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
    override fun execute(userText: String): Single<Boolean> {
        return screenPasswordValidator.isScreenPasswordCorrect(userText)
    }
}