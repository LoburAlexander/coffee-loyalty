package com.supersoft.coffeeloyalty.domain.validator

import io.reactivex.Single

/**
 * Created by alex.lobur on 22.12.2019.
 * Email: lobur.a.y@gmail.com
 */
interface ScreenPasswordValidator {

    fun isScreenPasswordCorrect(userText: String): Single<Boolean>
}